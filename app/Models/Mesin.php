<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Mesin extends Model
{
    use Uuids;
    public $increment = false;
    protected $guarded = ['id'];
    protected $table = 'machine';
    protected $fillable = ['machine_name', 'machine_type', 'machine_category', 'created_at', 'updated_at', 'deleted_at'];

}

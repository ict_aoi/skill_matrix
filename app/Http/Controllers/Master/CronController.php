<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;




class CronController extends Controller
{
    public function m_product(){
    	$date = $this->capdate();

    	$ln_prod = DB::connection('line')->table('master_product_header')
    							->whereBetween('created_at',$date)
    							->orwhereBetween('updated_at',$date)
    							->orwhereBetween('deleted_at',$date)
    							->get();


    	try {
    		DB::begintransaction();
    			foreach ($ln_prod as $prod) {
    				$cek = DB::table('m_product')->where('id',$prod->id)->whereNull('deleted_at')->first();

    				if ($cek==null) {
    					$in = array(
    						'id'=>$prod->id ,
    						'product_name'=>strtoupper($prod->nama_product),
    						'created_at'=> Carbon::now()
    					);

    					DB::table('m_product')->insert($in);
    				}else if ($cek!=null && $prod->deleted_at==null) {
    					$up = array(
    						'product_name'=>strtoupper($prod->nama_product),
    						'updated_at'=> Carbon::now()
    					);

    					DB::table('m_product')->where('id',$cek->id)->update($up);
    				}else{
    					DB::table('m_product')->where('id',$cek->id)->update(['deleted_at'=>Carbon::now()]);
    				}
    			}
    		DB::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    }


    public function m_style(){
    	$date = $this->capdate();

    	$ln_style = DB::connection('line')->table('master_product_detail')
    							->whereBetween('created_at',$date)
    							->orwhereBetween('updated_at',$date)
    							->orwhereBetween('deleted_at',$date)
    							->get();


    	try {
    		DB::begintransaction();
    			foreach ($ln_style as $sty) {
    				$cek = DB::table('m_style')->where('id',$sty->detail_id)->whereNull('deleted_at')->first();

    				if ($cek==null) {
    					$in = array(
    						'id'=>$sty->detail_id ,
    						'style_name'=>$sty->style,
    						'product_id'=>$sty->product_id,
    						'created_at'=> Carbon::now()
    					);

    					DB::table('m_style')->insert($in);
    				}else if ($cek!=null && $sty->deleted_at==null) {
    					$up = array(
    						'style_name'=>$sty->style,
    						'product_id'=>$sty->product_id,
    						'updated_at'=> Carbon::now()
    					);

    					DB::table('m_style')->where('id',$cek->id)->update($up);
    				}else{
    					DB::table('m_style')->where('id',$cek->id)->update(['deleted_at'=>Carbon::now()]);
    				}
    			}
    		DB::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    }

    public function m_machine(){


    	$ln_mech = DB::connection('line')->table('master_mesin')
    							->get();


    	try {
    		DB::begintransaction();
    			foreach ($ln_mech as $mech) {
    				$cek = DB::table('m_machine')->where('id',$mech->mesin_id)->whereNull('deleted_at')->first();

    				if ($cek==null) {
    					$in = array(
    						'machine_name'=>$mech->nama_mesin,
    						'machine_group'=>$mech->machine_group_name,
    						'machine_category'=> $mech->kategori_mesin,
    						'delay'=>$mech->machine_delay,
    						'created_at'=>Carbon::now(),
    						'id'=>$mech->mesin_id
    					);

    					DB::table('m_machine')->insert($in);
    				}else if ($cek!=null) {
    					$up = array(
    						'machine_name'=>$mech->nama_mesin,
    						'machine_group'=>$mech->machine_group_name,
    						'machine_category'=> $mech->kategori_mesin,
    						'delay'=>$mech->machine_delay,
    						'updated_at'=>Carbon::now(),
    					);

    					DB::table('m_machine')->where('id',$cek->id)->update($up);
    				}
    			}
    		DB::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    }

    public function m_complex(){
    	$date = $this->capdate();

    	$ln_comp = DB::connection('line')->table('master_complexity')
    							->whereBetween('created_at',$date)
    							->orwhereBetween('updated_at',$date)
    							->orwhereBetween('deleted_at',$date)
    							->get();


    	try {
    		DB::begintransaction();
    			foreach ($ln_comp as $comp) {
    				$cek = DB::table('m_complexity')->where('id',$comp->id)->whereNull('deleted_at')->first();

    				if ($cek==null) {
    					$in = array(
    						'id'=>$comp->id,
    						'complexity_name'=>$comp->complexity_name,
    						'created_at'=>Carbon::now()
    					);

    					DB::table('m_complexity')->insert($in);
    				}else if ($cek!=null && $comp->deleted_at==null) {
    					$up = array(
    						'complexity_name'=>$comp->complexity_name,
    						'updated_at'=>Carbon::now()
    					);

    					DB::table('m_complexity')->where('id',$cek->id)->update($up);
    				}else{

    					DB::table('m_complexity')->where('id',$cek->id)->update(['deleted_at'=>Carbon::now()]);
    				}
    			}
    		DB::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    }

    public function m_proses(){
    	$date = $this->capdate();

    	$ln_pros = DB::connection('line')->table('master_proses_new')
    							->whereBetween('created_at',$date)
    							->orwhereBetween('updated_at',$date)
    							->orwhereBetween('deleted_at',$date)
    							->get();


    	try {
    		DB::begintransaction();
    			foreach ($ln_pros as $pros) {
    				$cek = DB::table('m_process')->where('id',$pros->id)->whereNull('deleted_at')->first();

    				if ($cek==null) {
    					$in = array(
    						'id'=>$pros->id,
    						'id_complexity'=>$pros->id_complexity,
    						'id_machine'=>$pros->id_mesin,
    						'process_name'=>$pros->proses_name,
    						'created_at'=>Carbon::now(),
                            'id_product'=>$pros->id_product
    					);

    					DB::table('m_process')->insert($in);
    				}else if ($cek!=null && $pros->deleted_at==null) {
    					$up = array(
    						'id_complexity'=>$pros->id_complexity,
    						'id_machine'=>$pros->id_mesin,
    						'process_name'=>$pros->proses_name,
    						'updated_at'=>Carbon::now(),
                            'id_product'=>$pros->id_product
    					);

    					DB::table('m_process')->where('id',$cek->id)->update($up);
    				}else{

    					DB::table('m_process')->where('id',$cek->id)->update(['deleted_at'=>Carbon::now()]);
    				}
    			}
    		DB::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    }

    public function m_workflow(){
    	$date = $this->capdate();

    	$ln_wf = DB::connection('line')->table('master_workflow_new')
    							->whereBetween('create_date',$date)
    							->orwhereBetween('delete_at',$date)
    							->get();


    	try {
    		DB::begintransaction();
    			foreach ($ln_wf as $wf) {
    				$cek = DB::table('m_workflow')->where('id',$wf->master_workflow_id)->whereNull('deleted_at')->first();

    				if ($cek==null) {
    					$in = array(
    						'id'=>$wf->master_workflow_id,
    						'id_process'=>$wf->proses_id,
    						'id_style'=>$wf->product_detail_id,
                            'last_process'=>$wf->lastproses,
    						'gsd_smv'=>$wf->gsd_smv,
                            'cycle_time'=>$wf->cycle_time,
                            'smv_spt'=>$wf->smv_spt,
                            'is_critical'=>$wf->is_critical,
    						'created_at'=>Carbon::now()
    					);

    					DB::table('m_workflow')->insert($in);
    				}else if ($cek!=null && $pros->deleted_at==null) {
    					$up = array(
                            'id_process'=>$wf->proses_id,
                            'id_style'=>$wf->product_detail_id,
                            'last_process'=>$wf->lastproses,
                            'gsd_smv'=>$wf->gsd_smv,
                            'cycle_time'=>$wf->cycle_time,
                            'smv_spt'=>$wf->smv_spt,
                            'is_critical'=>$wf->is_critical,
                            'created_at'=>Carbon::now()
                        );


    					DB::table('m_workflow')->where('id',$cek->id)->update($up);
    				}else{

    					DB::table('m_process')->where('id',$cek->id)->update(['deleted_at'=>Carbon::now()]);
    				}
    			}
    		DB::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    }

    public function msewer(){
        $abs_sew = DB::connection('absensi')->table('adt_bbigroup_emp_new')
                                ->whereIN('factory',['AOI1','AOI2'])
                                ->where('department_name','Produksi')
                                ->where('position','Operator')
                                ->get();
        try {
            DB::begintransaction();
            foreach ($abs_sew as $sew) {
                $msew = DB::table('m_sewer')->where('nik',$sew->nik)->first();
                switch ($sew->factory) {
                    case 'AOI1':
                        $factory_id = 1;
                        break;

                    case 'AOI2':
                        $factory_id = 2;
                        break;


                }
                if ($msew==null) {
                    $data = array(
                        'nik'=>$sew->nik,
                        'name'=>strtoupper($sew->name),
                        'factory_id'=>$factory_id,
                        'created_at'=>Carbon::now()
                    );

                    DB::table('m_sewer')->insert($data);
                }
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function mline(){
        $mline = DB::connection('line')->table('master_line')
                                ->get();

        try {
            DB::begintransaction();
                foreach ($mline as $ml) {

                    $cek = DB::table('m_line')->where('id',$ml->master_line_id)->whereNull('deleted_at')->first();

                    if ($cek==null) {
                        $in = array(
                            'id'=>$ml->master_line_id,
                            'line_name'=>$ml->line_name,
                            'factory_id'=>$ml->factory_id,
                            'created_at'=>Carbon::now()
                        );

                        DB::table('m_line')->insert($in);
                    }
                }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

    }


    private function capdate(){
    	$data =array(
    		'from'=>Carbon::now()->format('Y-m-d 00:00:00'),
    		'to'=>Carbon::now()->format('Y-m-d 23:59:59')
    	);

    	return $data;
    }
}

<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;

class MasterController extends Controller
{
	public function __construct(){
	    ini_set('max_execution_time', 1800);
	}

    public function mmachine(){
        return view('master/master_machine');
    }

    public function getDataMachine(){
        $data = DB::table('m_machine')->whereNull('deleted_at')->orderBy('machine_name','asc');
        // $data = Mesin::wherenull('deleted_at')->orderBy('machine_name','asc');

    	return DataTables::of($data)
    			->make(true);
    }

    public function mprodstyle(){
    	return view('master/master_prodstyle');
    }

    public function getDataProdstle(){
    	$data = DB::table('m_style')
    					->join('m_product','m_product.id','=','m_style.product_id')
    					->whereNotNull('m_style.product_id')
    					->whereNull('m_style.deleted_at')
    					->whereNull('m_product.deleted_at')
    					->orderBy('m_style.created_at','desc');
    	return DataTables::of($data)
    						->make(true);
    }

    public function mproses(){
    	return view('master/master_process');
    }

    public function getDataProcess(){
    	$data = DB::table('m_process')
    					->join('m_machine','m_process.id_machine','=','m_machine.id')
    					->join('m_complexity','m_process.id_complexity','=','m_complexity.id')
                        ->join('m_product','m_process.id_product','=','m_product.id')
    					->whereNull('m_process.deleted_at')
    					->whereNull('m_machine.deleted_at')
    					->whereNull('m_complexity.deleted_at')
                        ->whereNull('m_product.deleted_at')
    					->orderBy('m_process.created_at','desc');

    	return DataTables::of($data)
    						->make(true);
    }

    public function mcomplexity(){
    	return view('master/master_complexity');
    }

    public function getDataComplex(){
    	$data = DB::table('m_complexity')
    					->whereNull('m_complexity.deleted_at')
    					->orderBy('m_complexity.created_at','desc');

    	return DataTables::of($data)
    						->make(true);
    }

    public function msewer(){
    	return view('master/master_sewer');
    }

    public function getDataSewer(){
    	$data = DB::table('m_sewer')
    					->whereNull('m_sewer.deleted_at')
    					->orderBy('m_sewer.created_at','desc');

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return $data->nik;
    						})
    						->rawColumns(['action'])
    						->make(true);
    }
    public function mqc(){
    	return view('master/master_qc');
    }

    public function getDataQC(){

        $id = 'AOI'.Auth::user()->factory_id;
    	$data = DB::table('adt_bbigroup_all_emp_new')
                        ->select('nik','name','department_name','subdept_name','position','factory')
                        ->where('factory',$id)
    					->get();

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return $data->nik;
    						})
    						->rawColumns(['action'])
    						->make(true);
    }

}

<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;
use response;

class ParameterController extends Controller
{
	public function __construct(){
	    ini_set('max_execution_time', 1800);
	}

    public function mparameter(){

        return view('/master/parameter/master_parameter');
    }

    public function getData(){

        $data = DB::table('m_parameter')
            ->get();

        // $section = DB::table('m_section')
        // ->where('id',$data->idsection)
        // ->get();

        return DataTables::of($data)
            ->addColumn('action', function($data){

                $btn = '<a class="btn btn-warning editdata" data-toggle="modal" data-target="#modal" style="margin-right: 10px;" data-id="'.$data->id.'" id="editdata">Detail</a>';

                $btn = $btn . '<a class="btn btn-danger deletedata" data-id="'.$data->id.'" id="deletedata">delete</a>';

                return $btn;
            })
            ->editColumn('idsection',function ($data)
            {

                    $change = DB::table('m_section')
                            ->where('id', $data->idsection)
                            ->first();

                    return $change->section;

            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function editParameter(Request $request){

        $data = DB::table('m_parameter')
            // ->join('m_section', 'm_parameter.idsection', '=', 'm_section.id')
            ->where('id', $request->idParameter)
            ->first();

        $section = DB::table('m_section')
                    ->select('id')
                    ->where('id',$data->idsection)
                    ->first();

        $data->idsection = $section->id;

        return response()->json($data);
    }

    public function editParameterSec(){


        $section = DB::table('m_section')
                    ->select('id','section')
                    ->get();

        return response()->json($section);
    }
    public function insertParameter(Request $request){

        DB::table('m_parameter')->insert(
            ['param' => $request->parameter, 'idsection' => $request->Section,
            'method'=>$request->method,'weight'=>$request->weight]
        );
    }
    public function updateParameter(Request $request){

        DB::table('m_parameter')
            ->where('id', $request->idParameter)
            ->update([
                'param' => $request->parameter,
                'method' => $request->method,
                'weight' => $request->weight,
                'idsection' => $request->Section
            ]);
    }

    public function deleteParameter(Request $request){

        DB::table('m_parameter')
            ->where('id', $request->id)
            ->delete();
    }

}

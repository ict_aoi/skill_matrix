<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;
use response;

class SectionController extends Controller
{
	public function __construct(){
	    ini_set('max_execution_time', 1800);
	}

    public function m_section(){
        return view('master/section/master_section');
    }

    public function getDataSection(){

        $data = DB::table('m_section')
            ->orderBy('id')
            ->get();

        return DataTables::of($data)
            ->addColumn('action', function($data){

                $btn = '<a class="btn btn-warning editdata" data-toggle="modal" data-target="#modal" style="margin-right: 10px;" data-id="'.$data->id.'" id="editdata">Detail</a>';

                $btn = $btn . '<a class="btn btn-danger deletedata" data-id="'.$data->id.'" id="deletedata">delete</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getDataCreateSection(){

        $data = DB::table('adt_bbigroup_all_emp_new')
            // ->table('adt_bbigroup_all_emp_new')
            ->where('department_name', '!=', null)
            ->distinct()
            ->get(['department_name']);

        // $param = DB::table('m_parameter')
        //     ->get();

        $data = [$data];

        return response()->json($data);
    }

    public function insertSection(Request $request){

        DB::table('m_section')
            ->insert([
                'section' => $request->sect,
                'department_name' => $request->department_name
            ]);

        // $section = DB::table('m_section')
        //         ->max('id');

        // foreach($request->param as $param){
        //     DB::table('m_section_parameter')
        //         ->insert([
        //             'idSection' => $section,
        //             'idParameter' => $param
        //         ]);
        // }
    }

    public function editSection(Request $request){

        $data = DB::table('m_section')
            ->where('id', $request->idSection)
            ->first();

        // $param = DB::table('m_section_parameter')
        //     ->where('idSection', $request->idSection)
        //     ->get();
        $dept = DB::table('adt_bbigroup_all_emp_new')
                ->where('department_name', '!=', null)
                ->distinct()
                ->get(['department_name']);
        // $dept = DB::connection('absensi')
        //     ->table('adt_bbigroup_all_emp_new')
        //     ->where('department_name', '!=', null)
        //     ->distinct()
        //     ->get(['department_name']);
        // $dept = DB::table('adt_bbigroup_all_emp_new')
        // $parameter = DB::table('m_parameter')
        //     ->get();

        // $data = [$data, $param, $dept, $parameter];

        return response()->json([$data, $dept]);
    }

    public function updateSection(Request $request){

        DB::table('m_section')
            ->where('id', $request->idSection)
            ->update([
                'section' => $request->sect,
                'department_name' => $request->dept
            ]);

        DB::table('m_section_parameter')
            ->where('idSection', $request->idSection)
            ->delete();

        if(!empty($request->param)){
            foreach($request->param as $param){
                DB::table('m_section_parameter')
                    ->insert([
                        'idSection' => $request->idSection,
                        'idParameter' => $param
                    ]);
            }
        }
    }

    public function deleteSection(Request $request){

        DB::table('m_section')
            ->where('id', $request->id)
            ->delete();
    }

}

<?php

namespace App\Http\Controllers\Input;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;
use Excel;

class ExcelController extends Controller
{
     public function getDocExcel($data){
        // return response()->json($data);
        $cus[]=array('Tanggal Buat','NIK','Nama','Nilai','Bobot','Huruf','Standar','Section','Parameter');


        foreach($data[0] as $a){

            if($a->idsection==20){
                if($a->param=='Testing method knowledge'){
                    switch($a->total){
                        case($a->total<=70):
                            $a->huruf = 'D';

                        break;
                        case($a->a>70 && $a->total<=80  ):
                            $a->huruf = 'C';
                        break;
                        case($a->total>80 && $a->total<=90  ):
                            $a->huruf = 'B';
                        break;
                        case($a->total>90  ):
                            $a->huruf = 'A';
                        break;
                        default:
                        return $a->total;

                    }
                }
                else{
                    switch($a->total){
                        case($a->total==4):
                            $a->huruf = 'D';
                        break;
                        case($a->total==5  ):
                            $a->huruf = 'C';
                        break;
                        case($a->total==6  ):
                            $a->huruf = 'B';
                        break;
                        case($a->total==7  ):
                            $a->huruf = 'A';
                        break;

                        default:
                        return $a->total;


                    }

                }

            }
            else{

                switch ($a->weight*100) {
                    case 10:
                        if($a->total<=7){
                            $a->huruf='C';
                        }

                        else if($a->total>7 && $a->total<=10 ){
                           $a->huruf='B';
                        }
                        else if($a->total==10 ){
                            $a->huruf='A';
                        }
                    break;
                    case 15:
                        if($a->total<=8){
                            $a->huruf='C';
                        }

                        else if($a->total>8 && $a->total<=10 ){
                            $a->huruf='B';
                        }
                        else if($a->total>10 ){
                            $a->huruf='A';
                        }
                    break;
                    case 20:
                        if($a->total<=15){
                            $a->huruf='C';
                        }

                        else if($a->total>15 && $a->total<=20 ){
                            $a->huruf='B';
                        }
                        else if($a->total>20 ){
                            $a->huruf='A';
                        }
                    break;
                    case 30:
                        if($a->total<=20){
                            $a->huruf='C';
                        }

                        else if($a->total>20 && $a->total<=25 ){
                            $a->huruf='B';
                        }
                        else if($a->total>25 ){
                            $a->huruf='A';
                        }
                    break;
                       case 40:
                        if($a->total<=30){
                            $a->huruf='C';
                        }

                        else if($a->total>30 && $a->total<=35 ){
                            $a->huruf='B';
                        }
                        else if($a->total>35 ){
                            $a->huruf='A';
                        }
                    break;

                    default:
                    $a->huruf='Tidak Terkonversi';


                }
            }
        }


        foreach($data[0] as $a){
            $cus[]=array(
                'Tanggal Buat' =>$a->created_at,
                'NIK'=>$a->nik,
                'Nama'=>$a->name,
                'Nilai'=>$a->total,
                'Bobot'=>$a->weight*100,
                'Huruf'=>$a->huruf,
                'Standar'=>'A',
                'Section'=>$a->section,
                'Parameter'=>$a->param,
            );
        }


        Excel::create('Report History '.Carbon::now()->isoFormat('DD-MM-YYYY H:mm:s'),function($excel)  use($cus) {
            $excel->setTitle('Datanya');
            $excel->sheet('General Site',function($sheet)  use($cus){
                // $sheet->cell('A1',  function($cell){
                //     $cell->setValue('Header');
                // });
                $sheet->fromArray($cus,null, 'A2', false, false);
            });
            // $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

      public function getExcel(Request $request){
        $NIK=$request->nik;
        $from=$request->from;
        $to=$request->to;
        $date_from = date_create($from);
        $date_to = date_create($to);
        $due_date = date_format($date_from,"Y-m-d");
        $due_date_to = date_format($date_to,"Y-m-d");


        $from = date($due_date);
        $to = date($due_date_to);


        $i=0;
        $datac=[];

        $from = strtotime($from);
        //convert bulannya $from
        $from = date("d-m-Y", $from);
        $bulan=substr($from,3,2);
        switch ($bulan) {
            case '01':
              $bulan='JAN';
              break;
            case '02':
              $bulan='FEB';
              break;
            case '03':
                $bulan='MAR';
                break;
            case '04':
                $bulan='APR';
                break;
            case '05':
                $bulan='MAY';
                break;
            case '06':
                $bulan='JUN';
                break;
            case '07':
                $bulan='JUL';
                break;
            case '08':
                $bulan='AGT';
                break;
            case '09':
                $bulan='SEP';
                break;
            case '10':
                $bulan='OKT';
                break;
            case '11':
                $bulan='NOV';
                break;
            case '12':
                $bulan='DES';
                break;
          }
        $day=substr($from,0,2);
        $year=substr($from,6,4);
        $from = $day.'-'.$bulan.'-'.$year;



        $to = strtotime($to);
        // Creating new date format from that timestamp
        $to = date("d-m-Y", $to);
        //convert bulannya $to
        $bulan=substr($to,3,2);
        switch ($bulan) {
            case '01':
              $bulan='JAN';
              break;
            case '02':
              $bulan='FEB';
              break;
            case '03':
                $bulan='MAR';
                break;
            case '04':
                $bulan='APR';
                break;
            case '05':
                $bulan='MAY';
                break;
            case '06':
                $bulan='JUN';
                break;
            case '07':
                $bulan='JUL';
                break;
            case '08':
                $bulan='AGT';
                break;
            case '09':
                $bulan='SEP';
                break;
            case '10':
                $bulan='OKT';
                break;
            case '11':
                $bulan='NOV';
                break;
            case '12':
                $bulan='DES';
                break;
          }
        $day=substr($to,0,2);
        $year=substr($to,6,4);
        $to = $day.'-'.$bulan.'-'.$year;





        if($NIK){
            if((Auth::user()->sub_department)=='Lab'){
                $data=DB::table('s_vwNilaiExcel')
                ->where('nik',$NIK)
                ->whereBetween('created_at',[$from, $to])
                ->where('idsection',20)
                ->orderBy('idsection','asc')
                ->get();
                // dd($data);
                array_push($datac,$data);
                return $this->getDocExcel($datac);
            }
            else{

                $data=DB::table('s_vwNilaiExcel')
                ->where('nik',$NIK)
                ->whereBetween('created_at',[$from, $to])
                ->orderBy('idsection','asc')
                ->get();
                // dd($data);
                array_push($datac,$data);
                return $this->getDocExcel($datac);
            }
        }



    }

    public function getTabelExcel(Request $request){

        $from=$request->from;
        $to=$request->to;

        $from = strtotime($from);
        //convert bulannya $from
        $from = date("d-m-Y", $from);
        $bulan=substr($from,3,2);
        switch ($bulan) {
            case '01':
              $bulan='JAN';
              break;
            case '02':
              $bulan='FEB';
              break;
            case '03':
                $bulan='MAR';
                break;
            case '04':
                $bulan='APR';
                break;
            case '05':
                $bulan='MAY';
                break;
            case '06':
                $bulan='JUN';
                break;
            case '07':
                $bulan='JUL';
                break;
            case '08':
                $bulan='AGT';
                break;
            case '09':
                $bulan='SEP';
                break;
            case '10':
                $bulan='OKT';
                break;
            case '11':
                $bulan='NOV';
                break;
            case '12':
                $bulan='DES';
                break;
          }
        $day=substr($from,0,2);
        $year=substr($from,6,4);
        $from = $day.'-'.$bulan.'-'.$year;



        $to = strtotime($to);
        // Creating new date format from that timestamp
        $to = date("d-m-Y", $to);
        //convert bulannya $to
        $bulan=substr($to,3,2);
        switch ($bulan) {
            case '01':
              $bulan='JAN';
              break;
            case '02':
              $bulan='FEB';
              break;
            case '03':
                $bulan='MAR';
                break;
            case '04':
                $bulan='APR';
                break;
            case '05':
                $bulan='MAY';
                break;
            case '06':
                $bulan='JUN';
                break;
            case '07':
                $bulan='JUL';
                break;
            case '08':
                $bulan='AGT';
                break;
            case '09':
                $bulan='SEP';
                break;
            case '10':
                $bulan='OKT';
                break;
            case '11':
                $bulan='NOV';
                break;
            case '12':
                $bulan='DES';
                break;
          }
        $day=substr($to,0,2);
        $year=substr($to,6,4);
        $to = $day.'-'.$bulan.'-'.$year;

        if((Auth::user()->sub_department)=='Lab'){
            $data = DB::table('s_vwNilaiExcel')
            ->where('nik', $request->nik)
            ->orderBy('created_at','desc')
            ->whereBetween('created_at',[$from, $to])
            ->where('idsection',20)
            ->orderBy('idsection','asc')
            ->get();
        }
        else{

            $data = DB::table('s_vwNilaiExcel')
            ->where('nik', $request->nik)
            ->orderBy('created_at','desc')
            ->whereBetween('created_at',[$from, $to])
            ->orderBy('idsection','asc')
            ->get();
        }

        // return $data;

        return DataTables::of($data)

                ->editColumn('huruf',function ($data)
                {
                    // return 'tes';
                    if($data->idsection==20){

                        $change = DB::table('m_parameter')
                        ->where('param', $data->param)
                        ->first();

                        if($change->param == 'Testing method knowledge'){
                            switch($data->total){
                                case($data->total<=70):
                                    $nilai = 'D';
                                    return $nilai;
                                break;
                                case($data->total>70 && $data->total<=80  ):
                                    $nilai = 'C';
                                    return $nilai;
                                break;
                                case($data->total>80 && $data->total<=90  ):
                                    $nilai = 'B';
                                    return $nilai;
                                break;
                                case($data->total>90  ):
                                    $nilai = 'A';
                                    return $nilai;
                                break;
                                default:
                                return $data->total;

                            }
                        }
                        else{
                            switch($data->total){
                                case($data->total==4):
                                    $nilai = 'D';
                                    return $nilai;
                                break;
                                case($data->total==5  ):
                                    $nilai = 'C';
                                    return $nilai;
                                break;
                                case($data->total==6  ):
                                    $nilai = 'B';
                                    return $nilai;
                                break;
                                case($data->total==7  ):
                                    $nilai = 'A';
                                    return $nilai;
                                break;

                                default:
                                return $data->nilai;


                            }

                        }


                    }
                    else{

                        switch ($data->weight*100) {
                            case 10:
                                if($data->total<=7){
                                    $change='C';
                                    return $change;
                                }

                                else if($data->total>7 && $data->total<=10 ){
                                    $change='B';
                                    return $change;
                                }
                                else if($data->total==10 ){
                                    $change='A';
                                    return $change;
                                }
                            break;
                            case 15:
                                if($data->total<=8){
                                    $change='C';
                                    return $change;
                                }

                                else if($data->total>8 && $data->total<=10 ){
                                    $change='B';
                                    return $change;
                                }
                                else if($data->total>10 ){
                                    $change='A';
                                    return $change;
                                }
                            break;
                            case 20:
                                if($data->total<=15){
                                    $change='C';
                                    return $change;
                                }

                                else if($data->total>15 && $data->total<=20 ){
                                    $change='B';
                                    return $change;
                                }
                                else if($data->total>20 ){
                                    $change='A';
                                    return $change;
                                }
                            break;
                            case 30:
                                if($data->total<=20){
                                    $change='C';
                                    return $change;
                                }

                                else if($data->total>20 && $data->total<=25 ){
                                    $change='B';
                                    return $change;
                                }
                                else if($data->total>25 ){
                                    $change='A';
                                    return $change;
                                }
                            break;
                               case 40:
                                if($data->total<=30){
                                    $change='C';
                                    return $change;
                                }

                                else if($data->total>30 && $data->total<=35 ){
                                    $change='B';
                                    return $change;
                                }
                                else if($data->total>35 ){
                                    $change='A';
                                    return $change;
                                }
                            break;

                            default:
                            $change='Tidak Terkonversi';

                        }
                    }




                })





                ->addColumn('weight', function($row){

                    // return '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->id_skill.'type="button" class="label label-info" class="btn btn-link legitRipple cekdetail">'.'LIHAT DETAIL'.'</a>';
                    $btn = $row->weight;

                    return $btn*100;
                })

                ->addColumn('standar', function($row){

                    // return '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->id_skill.'type="button" class="label label-info" class="btn btn-link legitRipple cekdetail">'.'LIHAT DETAIL'.'</a>';
                    $btn = 'A';

                    return $btn;
                })

                ->rawColumns(['huruf,weight,standar'])
                ->make(true);
    }



}

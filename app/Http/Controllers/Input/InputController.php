<?php

namespace App\Http\Controllers\Input;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;
use response;

class InputController extends Controller
{
	public function __construct(){
	    ini_set('max_execution_time', 1800);
	}

    public function index(){
        return view('Input/input_menu');
    }

    public function getDataQC(Request $request){

        if((Auth::user()->factory_id)==1){
            $factory='AOI1';
        }
        else if((Auth::user()->factory_id)==2){
            $factory='AOI2';
        }
        else{
            $factory='BBI';
        }

        	$data = DB::table('s_vwQC')
                            ->where('nik',$request->nik)
                            ->where('factory',$factory)
        					->get();

            return response()->json($data);
            // return $data;
            // dd($data);
        }
        public function getDataSection(){
        	$data = DB::table('m_section')

        					->get();
            return response()->json($data);
        }
        public function getDataParameter(Request $request){
        	$data = DB::table('m_parameter')
                            ->where('idsection',$request->id)
        					->get();

            $section = DB::table('m_section')
            ->where('id',$data[0]->idsection)
            ->first();

            return response()->json([$data,$section->section]);
        }

        public function InputForm(Request $request){

                $nik = $request->nik;
                $dataform=$request->dataform;
                $section=$request->section;
                $fruitbuah=$request->fruitbuah;

                $countnik=DB::table('skill')
                ->where('nik',$nik)
                ->count();

                if($countnik==0){

                                            DB::table('skill')
                                                ->insert(['nik'=>$nik,'idsection'=>$section,'created_at'=>now()]);

                                            $id_skill=DB::table('skill')
                                            ->max('id');

                                            $i=0;

                                            $comp = array_map('intval', $request->fruitbuah);


                                            foreach($dataform as $key => $value)
                                            {
                                                $a=$value['value'];
                                                $maximum=DB::table('m_parameter')
                                                   ->where('id',$comp[$i])
                                                   ->get();
                                                $weight=($maximum[0]->weight)*100;


                                               if($a>$weight){
                                                   $a=$weight;
                                               }
                                               else{
                                                   $a=$a;
                                               }


                                               DB::table('skill_detail')
                                               ->insert(['id_skill'=>$id_skill,'idparameter'=>$request->fruitbuah[$i],'nilai'=>$a,'created_at'=>now()]);
                                                $i=$i+1;
                                                // return response()->json(['success'=>'Score saved successfully.']);
                                            }

                    }

                else {

                    // $now=Carbon::now()->subDays(1);
                    // $now=$now->toDateString();
                    $now=Carbon::now()->toDateString();
                    $count=DB::table('skill')
                    ->whereDate('created_at', '=', $now)
                    ->where('nik',$nik)
                    ->where('idsection',$section)
                    ->count();

                    // echo $count;

                    if($count==0){
                        DB::table('skill')
                        ->insert(['nik'=>$nik,'idsection'=>$section,'created_at'=>now()]);

                        $id_skill=DB::table('skill')
                        ->max('id');

                        $i=0;

                        $comp = array_map('intval', $request->fruitbuah);


                        foreach($dataform as $key => $value)
                        {
                            $a=$value['value'];
                            $maximum=DB::table('m_parameter')
                            ->where('id',$comp[$i])
                            ->get();
                            $weight=($maximum[0]->weight)*100;


                            if($a>$weight){
                                $a=$weight;
                            }
                            else{
                                $a=$a;
                            }


                            DB::table('skill_detail')
                            ->insert(['id_skill'=>$id_skill,'idparameter'=>$request->fruitbuah[$i],'nilai'=>$a,'created_at'=>now()]);
                                $i=$i+1;
                            // return response()->json(['success'=>'Score saved successfully.']);
                        }
                    }
                    else{

                        // return response()->json(['Failed'=>'Anda sudah mengisi tadi']);
                        return response()->json(['error' => 'Data Anda sudah mengisi tadi'], 404);




                    }
                }




        }


    public function getHistoryData(){
        return view('Input/history');
    }


    public function getHistoryTrans(Request $request){
        $data = DB::table('skill')
        ->where('idsection',$request->id)
        ->get();
        return response()->json($data);
    }

    public function getSkill(Request $request){

        if((Auth::user()->sub_department)=='Lab'){
                 // s_vwNilaii & s_vwNilai
                 $data = DB::table('s_vwNilaii')
                 ->where('nik',$request->nik)
                 ->where('idsection','20');

                 return DataTables::of($data)
                 ->editColumn('idsection',function ($data)
                 {

                         $change = DB::table('m_section')
                                 ->where('id', $data->idsection)
                                 ->first();

                         return $change->section;

                 })
                 ->editColumn('total',function ($data)
                 {


                    if($data->idsection==20){
                        $dataLab = DB::table('s_vwDetailNilai')
                        ->select('nilai','weight')
                        ->where('nik',$data->nik)
                        ->where('id_skill',$data->sklid)
                        ->orderBy('idparameter','asc')
                        ->get();

                        $a = count($dataLab);
                        $total=0;
                        for($i=0;$i<$a;$i++){

                            if($i==0){

                                switch($dataLab[$i]->nilai){
                                    case($dataLab[$i]->nilai<=70):
                                        $nilai = 'D';
                                        $konversi = 1;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $total;
                                        // return $konversi;
                                    break;
                                    case($dataLab[$i]->nilai>70 && $dataLab[$i]->nilai<=80  ):
                                        $nilai = 'C';
                                        $konversi = 2;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;
                                    case($dataLab[$i]->nilai>80 && $dataLab[$i]->nilai<=90  ):
                                        $nilai = 'B';
                                        $konversi = 3;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;
                                    case($dataLab[$i]->nilai>90  ):
                                        $nilai = 'A';
                                        $konversi = 4;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;
                                    // default:
                                    // return $data->nilai;

                                }
                            }
                            else{
                                switch($dataLab[$i]->nilai){
                                    case($dataLab[$i]->nilai==4):
                                        $nilai = 'D';
                                        $konversi = 1;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;
                                    case($dataLab[$i]->nilai==5  ):
                                        $nilai = 'C';
                                        $konversi = 2;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;
                                    case($dataLab[$i]->nilai==6  ):
                                        $nilai = 'B';
                                        $konversi = 3;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;
                                    case($dataLab[$i]->nilai==7  ):
                                        $nilai = 'A';
                                        $konversi = 4;
                                        $total = $total+($konversi * $dataLab[$i]->weight);
                                        // return $konversi;
                                    break;

                                    // default:
                                    // return $data->nilai;


                                }
                            }
                            // $total=1;
                        }
                        switch($total){
                            case($total<=2):
                                $nilai = 'D';


                            break;
                            case($total>2 && $total<=2.75):
                                $nilai = 'C';
                            break;
                            case($total>2.75 && $total<=3.5):
                                $nilai = 'B';
                                // return $konversi;
                            break;
                            case($total>3.5):
                                $nilai = 'A';
                                // return $konversi;
                            break;

                            // default:
                            // return $data->nilai;


                        }

                        return $nilai;


                    }
                    else{
                            if($data->total<=60){
                                $change='C';
                                return $change;
                            }

                            else if($data->total>60 && $data->total<=79 ){
                                $change='B';
                                return $change;
                            }
                            else if($data->total>79 && $data->total<=100 ){
                                $change='A';
                                return $change;
                            }
                    }




                 })

                 ->addColumn('code', function($row){

                     // return '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->id_skill.'type="button" class="label label-info" class="btn btn-link legitRipple cekdetail">'.'LIHAT DETAIL'.'</a>';
                     $btn = '<button href="javascript:void(0)" type="button" data-toggle="tooltip"  data-id="'.$row->idsection.'" data-original-title="Delete" class="btn btn-secondary shadow btn-md js-exam-detail cekdetail">Lihat Detail</button>';

                     return $btn;
                 })

                 ->rawColumns(['code'])
                 ->make(true);
        }
        else{

                // s_vwNilaii & s_vwNilai
                $data = DB::table('s_vwNilaii')
                ->where('nik',$request->nik);

                return DataTables::of($data)
                ->editColumn('idsection',function ($data)
                {

                        $change = DB::table('m_section')
                                ->where('id', $data->idsection)
                                ->first();

                        return $change->section;

                })
                ->editColumn('total',function ($data)
                {




                        if($data->total<=60){
                            $change='C';
                            return $change;
                        }

                        else if($data->total>60 && $data->total<=79 ){
                            $change='B';
                            return $change;
                        }
                        else if($data->total>79 && $data->total<=100 ){
                            $change='A';
                            return $change;
                        }


                })





                ->addColumn('code', function($row){

                    // return '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->id_skill.'type="button" class="label label-info" class="btn btn-link legitRipple cekdetail">'.'LIHAT DETAIL'.'</a>';
                    $btn = '<button href="javascript:void(0)" type="button" data-toggle="tooltip"  data-id="'.$row->idsection.'" data-original-title="Delete" class="btn btn-secondary shadow btn-md js-exam-detail cekdetail">Lihat Detail</button>';

                    return $btn;
                })

                ->rawColumns(['code'])
                ->make(true);
        }
    }


    public function getSkillDetail(Request $request){
        $data = DB::table('s_vwDetailNilai')
        ->where('nik',$request->nik)
        ->where('idsection',$request->id_skill)
        ->orderBy('created_at','desc');



       return DataTables::of($data)
       ->editColumn('idparameter',function ($data)
       {

               $change = DB::table('m_parameter')
                       ->where('id', $data->idparameter)
                       ->first();

               return $change->param;

       })
       ->editColumn('standar',function ($data)
       {

               $persen = 'A';

               return $persen;

       })

       ->editColumn('nilai',function ($data)
        {
            // $tampung_nilai_lab=array();
            if($data->idsection==20){


                $change = DB::table('m_parameter')
                ->where('id', $data->idparameter)
                ->first();



                if($change->param == 'Testing method knowledge'){
                    switch($data->nilai){
                        case($data->nilai<=70):
                            $nilai = 'D';
                            return $nilai;
                        break;
                        case($data->nilai>70 && $data->nilai<=80  ):
                            $nilai = 'C';
                            return $nilai;
                        break;
                        case($data->nilai>80 && $data->nilai<=90  ):
                            $nilai = 'B';
                            return $nilai;
                        break;
                        case($data->nilai>90  ):
                            $nilai = 'A';
                            return $nilai;
                        break;
                        default:
                        return $data->nilai;

                    }
                }
                else{
                    switch($data->nilai){
                        case($data->nilai==4):
                            $nilai = 'D';
                            return $nilai;
                        break;
                        case($data->nilai==5  ):
                            $nilai = 'C';
                            return $nilai;
                        break;
                        case($data->nilai==6  ):
                            $nilai = 'B';
                            return $nilai;
                        break;
                        case($data->nilai==7  ):
                            $nilai = 'A';
                            return $nilai;
                        break;

                        default:
                        return $data->nilai;


                    }

                }


            }
            else{

                $weight=$data->weight;
                $weight=$weight*100;

                switch ($weight) {
                    case 10:
                        if($data->nilai<=7){
                            $change='C '.'('.$data->nilai.')';
                            return $change;
                        }

                        else if($data->nilai>7 && $data->nilai<=10 ){
                            $change='B '.'('.$data->nilai.')';
                            return $change;
                        }
                        else if($data->nilai==10 ){
                            $change='A '.'('.$data->nilai.')';
                            return $change;
                        }
                      break;
                      case 15:
                        if($data->nilai<=8){
                            $change='C '.'('.$data->nilai.')';
                            return $change;
                        }

                        else if($data->nilai>8 && $data->nilai<=10 ){
                            $change='B '.'('.$data->nilai.')';
                            return $change;
                        }
                        else if($data->nilai>10 ){
                            $change='A '.'('.$data->nilai.')';
                            return $change;
                        }
                      break;
                      case 20:
                        if($data->nilai<=15){
                            $change='C '.'('.$data->nilai.')';
                            return $change;
                        }

                        else if($data->nilai>15 && $data->nilai<=20 ){
                            $change='B '.'('.$data->nilai.')';
                            return $change;
                        }
                        else if($data->nilai>20 ){
                            $change='A '.'('.$data->nilai.')';
                            return $change;
                        }
                      break;
                      case 30:
                        if($data->nilai<=20){
                            $change='C '.'('.$data->nilai.')';
                            return $change;
                        }

                        else if($data->nilai>20 && $data->nilai<=25 ){
                            $change='B '.'('.$data->nilai.')';
                            return $change;
                        }
                        else if($data->nilai>25 ){
                            $change='A '.'('.$data->nilai.')';
                            return $change;
                        }
                      break;
                      case 40:
                        if($data->nilai<=30){
                            $change='C '.'('.$data->nilai.')';
                            return $change;
                        }

                        else if($data->nilai>30 && $data->nilai<=35 ){
                            $change='B '.'('.$data->nilai.')';
                            return $change;
                        }
                        else if($data->nilai>35 ){
                            $change='A '.'('.$data->nilai.')';
                            return $change;
                        }
                      break;
                    default:
                     return $data->nilai;
                  }
            }




        })

       ->addColumn('code', function($row){


           $btn = '<button href="javascript:void(0)" type="button" data-toggle="tooltip"  data-id="'.$row->id_skill.'" data-original-title="Delete" class="btn btn-secondary shadow btn-md js-exam-detail cekdetail">Lihat Detail</button>';

           return $btn;
       })

       ->rawColumns(['code'])
       ->make(true);
   }

    public function getReport(){
    	return view('input/report');
    }

    public function getNIK(){
        if((Auth::user()->factory_id)==1){
            $factory='AOI1';
        }
        else if((Auth::user()->factory_id)==2){
            $factory='AOI2';
        }
        else{
            $factory='BBI';
        }

        $data = DB::table('m_qc')
        ->where('factory',$factory)
        ->get();
        return response()->json($data);
    }


    public function getInputData(Request $request){

        // $absensi = DB::connection('absensi')
        //     ->table('adt_bbigroup_all_emp_new')
        //     ->where('nik', $request->nik)
        //     ->first();

            $absensi = DB::table('adt_bbigroup_all_emp_new')
            ->where('nik', $request->nik)
            ->first();

        $foto = DB::table('m_foto')
            ->where('nik', $request->nik)
            ->first();

        $sertif = DB::table('m_sertifikat')
            ->where('nik', $request->nik)
            ->get();
        if((Auth::user()->sub_department)=='Lab'){
            $section = DB::table('m_section')
                ->where('department_name','Lab')
                ->get();
        }
        else{

            $section = DB::table('m_section')
                ->get();
        }

        $nilai = DB::table('s_vwNilaii')
            ->where('nik', $request->nik)
            ->get();

        $data = [$absensi, $foto, $sertif, $section, $nilai];

        return response()->json($data);

    }



}

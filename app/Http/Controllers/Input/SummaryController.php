<?php

namespace App\Http\Controllers\Input;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;

class SummaryController extends Controller
{
	public function __construct(){
	    ini_set('max_execution_time', 1800);
	}

    public function index(){
        return view('Input/Summary');
    }
    public function getDataParameter(Request $request){
        $count = DB::table('m_parameter')
            ->count('param')
            ->where ('idsection',4);
            return response()->json($request->k);


    }

    public function getSummary(Request $request){
        //1b & 2b
        if((Auth::user()->factory_id)==1){
            $factory='AOI1';
        }
        else if((Auth::user()->factory_id)==2){
            $factory='AOI2';
        }
        else{
            $factory='BBI';
        }
        // 1c & 2b
        $data = DB::table('s_vwGetSummary2b')
        ->where('factory',$factory)
        //->paginate(1);
         ->get();
        return response()->json($data);
    }



}

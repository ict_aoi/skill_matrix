<?php

namespace App\Http\Controllers\Input;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Mesin;
use Illuminate\Support\Facades\File;
// use Validator;


class UploadController extends Controller
{
    public function upload()
    {
        $gambar = DB::table('m_foto')
            ->get();
        return view('Input/upload', compact('gambar'));
    }

    public function TampilFoto(Request $request)
    {

        $count = DB::table('m_foto')
            ->where('nik', $request->nik)
            ->count();



        if ($count == 0) {

            return response()->json('man.jpg');
        } else {
            // $gambar=avatar.jpg;
            $gambar = DB::table('m_foto')
                ->select('file')
                ->where('nik', $request->nik)
                ->get();

            return response()->json($gambar);
        }
    }

    public function proses_upload(Request $request)
    {
        // dd($request->all());

        if ($request->filee) {
            $this->validate($request, [
                'filee' => 'required|mimes:jpg,jpeg|max:5120',
            ]);

            $file = $request->file('filee');
            // nama file
            $nama_file = time() . "-" . $file->getClientOriginalName();

            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';

            // upload file
            $file->move($tujuan_upload, $nama_file);

            $data =   DB::table('m_foto')
                ->where('nik', $request->nik)
                ->first();

            if ($data) {

                $destinationPath = 'data_file';
                File::delete($destinationPath.'/'.$data->file);

                DB::table('m_foto')
                    ->where('nik', $request->nik)
                    ->update(['file' => $nama_file, 'updated_at' => now()]);
            } else {

                DB::table('m_foto')
                    ->insert(['file' => $nama_file, 'created_at' => now(), 'nik' => $request->nik]);
            }
        }

        //KHUSUS SERTIF

        $countSertif = DB::table('m_sertifikat')
            ->where('nik', $request->nik)
            ->count();

        if($countSertif < 5){
            if ($request->file and $request->keterangan) {

                $this->validate($request, [
                    'file' => 'required|mimes:jpg,jpeg|max:5120',
                ]);

                // dd($request->file);
                // upload sertif
                $keterangan = $request->keterangan;
                // $keterangan = explode(",",$keterangan[0]);
                $files = $request->file;
                // menyimpan data file yang diupload ke variabel $file
                $file = $request->file('file');

                // isi dengan nama folder tempat kemana file diupload
                $tujuan_upload = 'sertif';

                // Upload Orginal Image
                $nama_file = time() . "_" . $file->getClientOriginalName();
                $file->move($tujuan_upload, $nama_file);


                // Save In Database
                DB::table('m_sertifikat')
                    ->insert([
                        'file' => $nama_file,
                        'keterangan' => $keterangan,
                        'created_at' => now(),
                        'nik' => $request->nik
                    ]);
            }
        }

        // return view('Input/input_menu');
        return redirect('/input/input-form');
    }

    public function proses_upload_sertif(Request $request)
    {
        // $this->validate($request, [
        // 	'file' => 'required'
        // ]);
        // dd($request->all());
        $keterangan = $request->keterangan;
        $keterangan = explode(",", $keterangan[0]);
        $files = $request->file;


        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');


        // nama file


        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $i = 0;
        foreach ($file as $img) {
            // Upload Orginal Image
            $nama_file = time() . "_" . $img->getClientOriginalName();
            $img->move($tujuan_upload, $nama_file);


            // Save In Database
            DB::table('m_sertifikat')
                ->insert(['file' => $nama_file, 'keterangan' => $keterangan[$i], 'created_at' => now(), 'nik' => $request->nik]);
            $i = $i + 1;
        }



        return view('Input/history');
    }

    public function TampilSertif(Request $request)
    {

        $count = DB::table('m_sertifikat')
            ->where('nik', $request->nik)
            ->count();



        if ($count == 0) {
            // $gambar = DB::table('m_foto')
            // ->select('file')
            // ->where('nik',$request->nik)
            // ->get();

            return response()->json('Belum Upload Sertifikat');
        } else {
            // $gambar=avatar.jpg;
            $keterangan = DB::table('m_sertifikat')
                ->select('keterangan,')
                ->where('nik', $request->nik)
                ->get();

            return response()->json($keterangan);
        }
    }
    public function HapusSertif($id)
    {

        $nik = DB::table('m_sertifikat')
        ->where('id', $id)
        ->first();


        $destinationPath = 'sertif';
        File::delete($destinationPath.'/'.$nik->file);

        DB::table('m_sertifikat')
            ->where('id', $id)
            ->delete();

        $data = DB::table('m_sertifikat')
            ->where('nik', $nik->nik)
            ->get();

        return response()->json($data);

        // return redirect()->route('input.index');
    }

    public function DetailSertif(Request $request){
        $data = DB::table('m_sertifikat')
            ->where('id', $request->id)
            ->first();

        return response()->json($data);
    }
}

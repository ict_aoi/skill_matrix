<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'ICT',
            'display_name' => 'ICT',
            'description' => 'ICT',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('permissions')->insert([
            'name' => 'dashboard',
            'display_name' => 'menu-dashboard',
            'description' => 'menu dashboard',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'master',
            'display_name' => 'menu-master',
            'description' => 'menu master',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

       
        DB::table('users')->insert([
            'name' => 'ICT Admin',
            'nik' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('password'),
            'factory_id' => '2',
            'admin_role' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
    }
}

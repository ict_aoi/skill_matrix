@extends('layouts.app', ['active' => 'master_section'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Master Section</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">

                {{-- Input Section --}}
                <div class="row">
                    <a id="buttonAddSection" class="btn btn-success" data-toggle="modal" data-target="#modal">
                        <i class="icon-plus2"></i> Add Section
                    </a>
                </div>

                {{-- Table Section --}}
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>Department</th>
									<th>Section</th>
                                    <th>Detail</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

                {{-- Modal --}}
                <div class="modal fade text-left" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 90%;" role="dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="" id="modalLabel">Section</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="icon-cross"></i>
                                </button>
                            </div>

                            <div class="modal-body">

                                <div class="row">

                                    {{-- Left Panel --}}
                                    <div class="col-md-6">

                                        {{-- Department --}}
                                        <div class="row">

                                            <input type="number" id="idSection" hidden>

                                            <div class="col-md-6">
                                                <br>
                                                <label>Select Department :</label>
                                            </div>

                                            <div class="col-md-6">
                                                <select class="form-control" name="selectDepartment" id="selectDepartment"><br>
                                                    {{-- <input type="text" class="form-control" name="selectDepartment" id="selectDepartment" value=""> --}}

                                                </select>
                                            </div>

                                        </div>

                                        {{-- Section --}}
                                        <div class="row">

                                            <div class="col-md-4">
                                                <br>
                                                <label>Section :</label>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="inputSection" id="inputSection"  value="">
                                            </div>

                                        </div>

                                    </div>

                                    {{-- Right Panel --}}
                                    <div class="col-md-6">

                                        <div class="row" id="paramCheckbox" style="height: 60vh;overflow-y: scroll;">
                                        </div>

                                    </div>

                                    {{-- Button Panel --}}
                                    <div class="col-md-12 text-center">
                                        <br>
                                        <button id="buttonSave" class="btn btn-info">
                                            Create Section
                                        </button>
                                        <button id="buttonUpdate" class="btn btn-info">
                                            Update Section
                                        </button>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
    $(document).ready(function(){

        // Ajax Setup
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('#table-list').DataTable({
            processing:true,
            serverSide:true,
            deferRender:true,
            dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('master.getDataSection') }}"
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'department_name', name: 'department_name'},
                {data: 'section', name: 'section'},
                {data: 'action', name: 'action'}
            ]
        });

        $('#buttonAddSection').click(function () {
            $('#buttonUpdate').hide();
            $('#buttonSave').show();
            // $('#selectDepartment').val('');
            $('#inputSection').val('');

            $.ajax({
                type: 'get',
                url: "{{ route('master.getDataCreateSection') }}",
                success: function(data){
                    console.log('oke',data)
                    var dept = $('#selectDepartment');
                    dept.empty();
                    dept.append('<option value="" selected disable hidden>-- Choose Department --</option>');
                    for (var i = 0; i < data[0].length; i++){
                        dept.append('<option value="'+data[0][i].department_name+'">'+data[0][i].department_name+'</option>');
                    }

                    // var param = $('#paramCheckbox');
                    // param.empty();
                    // for(var i = 0; i < data[1].length; i++){
                    //     param.append('<div class="col-md-6"><input name="parameter" type="checkbox" value="'+data[1][i].id+'"><label> &emsp; '+data[1][i].param+'</label></div>');
                    // }
                },
                error: function(data){
                    console.log('Error', data);
                }
            })
        })

        $('#buttonSave').click(function(){
            var dept = $('#selectDepartment').val();
            var sect = $('#inputSection').val();
            var param = [];
            $('input[name="parameter"]:checked').each(function(i){
                param[i] = $(this).val();
            });

            if(dept == '' || sect == ''){
                // Notif error
                console.log('Data Masih Kosong');
            }

            console.log('Data : '+ dept +  sect);
            console.log('Data Parameter : '+ param);

            $.ajax({
                type: "POST",
                url: "{{ route('master.insertSection') }}",
                data: {
                    dept: dept,
                    sect: sect
                    // param: param
                },
                beforeSend: function() {
                            $.blockUI({
                                baseZ: 2000,
                                message: '<i class="icon-spinner9 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                success: function(data){
                    $('#buttonUpdate').show();
                    $('#buttonSave').hide();
                    table.draw();
                },
                error: function(data){
                    console.log('Error', data);
                }
            })
        })

        $('body').on('click', '.editdata', function(){
            // console.log('Edit Data Clicked');
            $('#buttonUpdate').show();
            $('#buttonSave').hide();
            var idSection = $(this).data('id');

            $.ajax({
                type: 'post',
                url: "{{ route('master.editSection') }}",
                data: {
                    idSection: idSection
                },
                beforeSend: function() {
                            $.blockUI({
                                baseZ: 2000,
                                message: '<i class="icon-spinner9 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                success: function(data){

                    $('#idSection').val(data[0].id);

                    console.log('Data percobaan : ', data);
                    // Section
                    $('#inputSection').val(data[0].section);

                    // Department
                    var dept = $('#selectDepartment');
                    dept.empty();
                    dept.append('<option value="'+ data[0].department_name +'" selected hidden>'+ data[0].department_name +'</option>');
                    for (var i = 0; i < data[1].length; i++){
                        dept.append('<option value="'+data[1][i].department_name+'">'+data[1][i].department_name+'</option>');
                    }

                    // Parameter
                    // var param = $('#paramCheckbox');
                    // param.empty();
                    // for(var i = 0; i < data[3].length; i++){
                    //     if(data[1].length > 0){
                    //         for(var j = 0; j < data[1].length; j++){
                    //             if(data[3][i].id == data[1][j].idParameter){
                    //                 param.append('<div class="col-md-6"><input name="parameter" type="checkbox" value="'+data[3][i].id+'" checked><label> &emsp; '+data[3][i].param+'</label></div>');
                    //             }
                    //             else{
                    //                 param.append('<div class="col-md-6"><input name="parameter" type="checkbox" value="'+data[3][i].id+'"><label> &emsp; '+data[3][i].param+'</label></div>');
                    //             }
                    //         }
                    //     }
                    //     else{
                    //         param.append('<div class="col-md-6"><input name="parameter" type="checkbox" value="'+data[3][i].id+'"><label> &emsp; '+data[3][i].param+'</label></div>');
                    //     }
                    // }
                },
                error: function(data){
                    console.log('Error', data);
                }
            })
        })

        $('#buttonUpdate').click(function(){
            var dept = $('#selectDepartment').val();
            var sect = $('#inputSection').val();
            var param = [];
            $('input[name="parameter"]:checked').each(function(i){
                param[i] = $(this).val();
            });
            var idSection = $('#idSection').val();

            if(dept == '' || sect == ''){
                // Notif error
                console.log('Data Masih Kosong');
            }

            console.log('Data : '+ dept +  sect);
            console.log('Data Parameter : '+ param);

            $.ajax({
                type: "POST",
                url: "{{ route('master.updateSection') }}",
                data: {
                    dept: dept,
                    sect: sect,
                    idSection: idSection

                },
                beforeSend: function() {
                            $.blockUI({
                                baseZ: 2000,
                                message: '<i class="icon-spinner9 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                success: function(data){
                    table.draw();
                },
                error: function(data){
                }
            })
        })

        $('body').on('click', '.deletedata', function(){

            Swal.fire({
                title: 'Delete ?',
                icon: 'question',
                timer: 2000,
                timerProgressBar: true,
                showDenyButton: true,
                showCancelButton: false,
                confirmButtonText: `Delete`,
                denyButtonText: `Don't Delete`,
                }).then((result) => {
                    if (result.isConfirmed) {

                        var id = $(this).data("id");

                        $.ajax({
                            type: "post",
                            url: "{{ route('master.deleteParameter') }}",
                            data:{
                                id: id
                            },
                            success: function (data) {
                                table.draw();
                                Swal.fire({
                                    title: 'Deleted',
                                    icon: 'success',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    position: 'bottom-end',
                                    showConfirmButton: false,
                                });
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });
                    } else if (result.isDenied) {
                    Swal.fire('Canceled', '', 'info')
                }
            });
        })

    });
</script>
@endsection

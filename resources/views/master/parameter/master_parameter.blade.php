@extends('layouts.app', ['active' => 'master_parameter'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Master Parameter</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">

                {{-- Input Section --}}
                <div class="row">
                    <a id="buttonAddSection" class="btn btn-success" data-toggle="modal" data-target="#modal">
                        <i class="icon-plus2"></i> Add Parameter
                    </a>
                </div>

                {{-- Table Section --}}
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>Parameter</th>
									<th>Method</th>
                                    <th>Weight</th>
                                    <th>Section</th>
                                    <th>Detail</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

                {{-- Modal --}}
                <div class="modal fade text-left" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="" id="modalLabel">Parameter</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="icon-cross"></i>
                                </button>
                            </div>

                            <div class="modal-body">

                                <div class="row">

                                    {{-- Left Panel --}}
                                    <div class="col-md-12">

                                        <input type="number" id="idParameter" value="" hidden>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <br>
                                                <label>Parameter :</label>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="inputParameter" id="inputParameter"
                                                value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <br>
                                                <label>Method :</label>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="inputMethod" id="inputMethod"
                                                value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <br>
                                                <label>Weight :</label>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="number" class="form-control" name="inputWeight" id="inputWeight"
                                                value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <br>
                                                <label>Section :</label>
                                            </div>

                                            <div class="col-md-6">
                                                <select class="form-control" name="selectSection" id="selectSection">

                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    {{-- Button Panel --}}
                                    <div class="col-md-12 text-center">
                                        <br>
                                        <button id="buttonSave" class="btn btn-info">
                                            Create Section
                                        </button>
                                        <button id="buttonUpdate" class="btn btn-info">
                                            Update Section
                                        </button>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
    $(document).ready(function(){

        // Ajax Setup
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('#table-list').DataTable({
            processing:true,
            serverSide:true,
            deferRender:true,
            dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('master.getDataParameter') }}"
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'param', name: 'param'},
                {data: 'method', name: 'method'},
                {data: 'weight', name: 'weight'},
                {data: 'idsection', name: 'idsection'},
                {data: 'action', name: 'action'}
            ]
        });

        $('#buttonAddSection').click(function () {
            $('#buttonUpdate').hide();
            $('#buttonSave').show();
            $('#inputParameter').val('');
            $('#inputMethod').val('');
            $('#inputWeight').val('');
            $('#selectSection').empty();
            $.ajax({
                    type: 'post',
                    url: "{{ route('master.editParameterSec') }}",
                    success: function(data){
                        console.log('Success', data);
                        $('#selectSection').empty();
                                    $('#selectSection').append('<option' + '>' +
                                        'Silakan Pilih Section' + '</option>');
                        // console.log(data.idsection);
                                    for (var i = 0; i < data.length; i++) {
                                        // if(idsection==data[i].id ){
                                        console.log(data[i].id)


                                            $('#selectSection').append('<option value=' + data[i].id + '>' +
                                            data[i].section + '</option>');


                                    }

                    },
                    error: function(data){
                        console.log('Error', data);
                    }
                })
        })

        $('#buttonSave').click(function(){
            var idParameter = $('#idParameter').val();
            var parameter = $('#inputParameter').val();
            var method = $('#inputMethod').val();
            var weight = $('#inputWeight').val();
            var Section = $('#selectSection').val();

            // if(parameter == '' || method == '' || weight == '' || Section == ''){
            //     // Notif error
            //     console.log('Data Masih Kosong');
            // }
            console.log(parameter)

            $.ajax({
                type: "POST",
                url: "{{ route('master.insertParameter') }}",
                data: {
                    idParameter: idParameter,
                    parameter: parameter,
                    method:method,
                    weight:weight,
                    Section: Section
                },
                beforeSend: function() {
                                $.blockUI({
                                    baseZ: 2000,
                                    message: '<i class="icon-spinner9 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function() {
                                $.unblockUI();
                            },
                success: function(data){
                    $('#buttonUpdate').show();
                    $('#buttonSave').hide();
                    table.draw();
                },
                error: function(data){
                    console.log('Error', data);
                }
            })
        })

        $('body').on('click', '.editdata', function(){
            $('#buttonUpdate').show();
            $('#buttonSave').hide();
            var idParameter = $(this).data('id');

            $.ajax({
                type: 'post',
                url: "{{ route('master.editParameter') }}",
                data: {
                    idParameter: idParameter
                },
                success: function(data){
                    console.log('Success', data);

                    $('#idParameter').val(data.id);
                    $('#inputParameter').val(data.param);
                    $('#inputMethod').val(data.method);
                    $('#inputWeight').val(data.weight);
                    $('#inputStandar').val(data.standar);
                    // $('#selectSection').empty();
                    //             $('#selectSection').append('<option value=' + 0 + '>' +
                    //                 'Silakan Pilih Section' + '</option>');
                    var idsection = data.idsection;

                    dropdownAppend(idsection);
                                // for (var i = 0; i < data.length; i++) {

                                //     $('#selectSection').append('<option value=' + data[i].id + '>' +
                                //         data[i].section + '</option>');

                                // }
                },
                error: function(data){
                    console.log('Error', data);
                }
            })


            function dropdownAppend (idsection){

                $.ajax({
                    type: 'post',
                    url: "{{ route('master.editParameterSec') }}",
                    success: function(data){
                        console.log('Success', data);

                        // $('#idParameter').val(data.id);
                        // $('#inputParameter').val(data.param);
                        // $('#inputMethod').val(data.method);
                        // $('#inputWeight').val(data.weight);
                        // $('#inputStandar').val(data.standar);
                        $('#selectSection').empty();
                                    $('#selectSection').append('<option' + '>' +
                                        'Silakan Pilih Section' + '</option>');
                        // console.log(data.idsection);
                                    for (var i = 0; i < data.length; i++) {
                                        if(idsection==data[i].id ){
                                        console.log(data[i].id)

                                        $('#selectSection').append('<option selected="selected" value=' + data[i].id + '>' +
                                            data[i].section + '</option>');

                                        }
                                        else{
                                            $('#selectSection').append('<option value=' + data[i].id + '>' +
                                            data[i].section + '</option>');
                                        }

                                    }

                    },
                    error: function(data){
                        console.log('Error', data);
                    }
                })
            }


        })

        $('#buttonUpdate').click(function(){
            var idParameter = $('#idParameter').val();
            var parameter = $('#inputParameter').val();
            var method = $('#inputMethod').val();
            var weight = $('#inputWeight').val();
            var Section = $('#selectSection').val();

            $.ajax({
                type: "POST",
                url: "{{ route('master.updateParameter') }}",
                data: {
                    idParameter: idParameter,
                    parameter: parameter,
                    method: method,
                    weight: weight,
                    Section: Section
                },
                beforeSend: function() {
                                $.blockUI({
                                    baseZ: 2000,
                                    message: '<i class="icon-spinner9 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function() {
                                $.unblockUI();
                            },
                success: function(data){
                    table.draw();
                },
                error: function(data){
                }
            })
        })

        $('body').on('click', '.deletedata', function(){

            Swal.fire({
                title: 'Delete ?',
                icon: 'question',
                timer: 2000,
                timerProgressBar: true,
                showDenyButton: true,
                showCancelButton: false,
                confirmButtonText: `Delete`,
                denyButtonText: `Don't Delete`,
                }).then((result) => {
                    if (result.isConfirmed) {

                        var id = $(this).data("id");

                        $.ajax({
                            type: "post",
                            url: "{{ route('master.deleteParameter') }}",
                            data:{
                                id: id
                            },
                            success: function (data) {
                                table.draw();
                                Swal.fire({
                                    title: 'Deleted',
                                    icon: 'success',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    position: 'bottom-end',
                                    showConfirmButton: false,
                                });
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });
                    } else if (result.isDenied) {
                    Swal.fire('Canceled', '', 'info')
                    }
                });
            })

    });
</script>
@endsection

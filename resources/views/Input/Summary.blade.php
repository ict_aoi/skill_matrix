@extends('layouts.app', ['active' => 'summary'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Input</a></li>
			<li class="active">Summary</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">

		<div class=" panel panel-flat">
            <div class="panel-body">
                <button id="btnExport" > Download Report </button>


                    <h2>Select Number Of Rows</h2>
                            <div class="form-group">
                                 <select class  ="form-control" name="state" id="maxRows">
                                     <option value="5000">Show ALL Rows</option>
                                     <option value="6">2</option>
                                     <option value="12">4</option>
                                     <option value="18">6</option>
                                     <option value="24">8</option>
                                     <option value="30">10</option>
                                     <option value="36">12</option>
                                    </select>

                              </div>
                {{-- </div> --}}
                <div class="table-responsive">
                    <table id="example" class="display table table-bordered " style="width:100%">

                        {{-- <thead style="background-color:#ebe6e6;"> --}}
                            <thead>
                            <tr>
                                <th  rowspan="3" ><b>Employee Name<b></th>
                                <th  rowspan="3"><b>Section in Charge<b></th>
                                <th  rowspan="3"><b>Skill Level<b></th>
                                <th  colspan="5"  class="text-center"><b>Sewing Inspector</b></th>
                                <th  colspan="7" class="text-center"><b>Fabric Inspector</b></th>
                                <th  colspan="5" class="text-center"><b>Accesories Inspector</b></th>
                                <th  colspan="4" class="text-center"><b>PPA Inspector</b></th>
                                <th  colspan="6" class="text-center"><b>Cutting Inspector</b></th>
                                <th  colspan="6" class="text-center"><b>Artwork Inspector</b></th>
                                <th  colspan="7" class="text-center"><b>Quality Auditor</b></th>
                            </tr>
                        <tr>

                            <th ><b>Understanding of defect </th>
                            <th ><b>Time Study </th>
                            <th ><b>Measurement</th>
                            <th ><b>Clockwise System </th>
                            <th  rowspan="2"><b>Total Score</th>

                            <th ><b>Understanding of defect </th>
                            <th ><b>Color Inspection </th>
                            <th ><b>Inspection Method (4 point system)</th>
                            <th ><b>Fabric width measurement </th>
                            <th ><b>Odour Checking</th>
                            <th ><b>Handfeel Checking</th>
                            <th  rowspan="2"><b>Total Score</th>

                                <th ><b>Understanding of defect </th>
                                <th ><b>Measurement </th>
                                <th ><b>Time study</th>
                                <th ><b>Approval Comparation </th>
                                <th  rowspan="2"><b>Total Score</th>


                                <th ><b>Understanding of defect </th>
                                <th ><b>Time study</th>
                                <th ><b>Measurement</th>
                                <th  rowspan="2"><b>Total Score</th>

                                    <th ><b>Understanding of defect </th>
                                    <th ><b>Measurement</th>
                                    <th ><b>Time study</th>
                                    <th ><b>Surface fabric</th>
                                    <th ><b>Spreading Quality</th>
                                    <th  rowspan="2"><b>Total Score</th>


                                        <th ><b>Understanding of defect </th>
                                            <th ><b>Measurement</th>
                                            <th ><b>Time study</th>
                                            <th ><b>Approval Comparation</th>
                                            <th ><b>Color inspection</th>
                                            <th  rowspan="2"><b>Total Score</th>

                                                <th ><b>First output checking </th>
                                                    <th ><b>Bulk Order Inspection</th>
                                                    <th ><b>AD & Documents</th>
                                                    <th ><b>Sample Order Inspection</th>
                                                    <th ><b>General Knowledge</th>
                                                        <th ><b>Decision & Escalation</th>
                                                    <th  rowspan="2"><b>Total Score</th>


                        </tr>
                        <tr>

                            <th >30 </th>
                            <th >20 </th>
                            <th >20</th>
                            <th >30 </th>
                            <th >30 </th>
                            <th >20 </th>
                            <th >20 </th>
                            <th >10 </th>
                            <th >10 </th>
                            <th >10 </th>
                            <th >30 </th>
                            <th >30 </th>
                            <th >20 </th>
                            <th >20</th>
                            <th >40 </th>
                            <th >30 </th>
                            <th >30 </th>
                            <th >30 </th>
                            <th >20 </th>
                            <th >20 </th>
                            <th >15 </th>
                            <th >15 </th>
                            <th >30 </th>
                            <th >20 </th>
                            <th >20 </th>
                            <th >15 </th>
                            <th >15 </th>
                            <th >20 </th>
                            <th >20 </th>
                            <th >20 </th>
                            <th >10 </th>
                            <th >10 </th>
                            <th >20 </th>


                        </tr>
                        </thead>
                        <tbody id="table-body" >


                        </tbody>

                    </table>
                </div>
                <br>
                <div class="pagination-container">
                    <nav>
                        <ul class="pagination"></ul>
                    </nav>
                </div>

	        </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){

    document.getElementById("btnExport").onclick = function() {fnExcelReport()};

    function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('example'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";

    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
}
    $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });

        $.ajax({
	        type: 'get',
	        url : "{{ route('input.getSummary') }}",
            // data: {nik:nik},

	        success: function(data) {
	        //     console.log('oke');
                // console.log(data);
                var tempsec;
                for (var i = 0; i < data.length; i++) {
                    $('#table-body').append('<tr><td rowspan="3">' + data[i].nama + '</td>'+'<td rowspan="3">' + section(data[i].section) +
                    '</td>'+'<td>' + 'Standard' + '</td>'+td(data[i].nilai,data[i].section,data[i].parameter,data[i].weight) );

                    function section(section){
                        section=section.split(',');
                        var a='', tempsec;
                        //cek data pertama sampai trakhir
                        for(var k=0; k<section.length; k++){

                            //jika sama dengan sebelumnya
                            if(tempsec==section[k]){

                            }

                            //jika beda
                            else{
                                tempsec=section[k];


                                    if(section[k]==1){
                                        a=a+'Sewing, ';
                                    }
                                    else if(section[k]==2){
                                        a=a+'Fabric, ';
                                    }
                                    else if(section[k]==3){
                                        a=a+'Accesories , ';
                                    }
                                    else if(section[k]==4){
                                        a=a+'PPA , ';
                                    }
                                    else if(section[k]==5){
                                        a=a+'Cutting , ';
                                    }
                                    else if(section[k]==6){
                                        a=a+'Artwork , ';
                                    }
                                    else if(section[k]=7){
                                        a=a+'Quality Auditor';
                                    }

                            }


                        }

                            return a;

                    }

                    function td(nilai, section, parameter, weight){
                        var a;
                        nilai=nilai.split(',');
                        section=section.split(',');
                        parameter=parameter.split(',');
                        weight=weight.split(',');
                        nilai = nilai.map(Number)

                        parameter = parameter.map(Number)

                        var result = {};
                        parameter.forEach((key, i) => result[key] = nilai[i]);


                        var keys = [];
                        var values = [];

                        for (var key in result) {
                            if (result.hasOwnProperty(key)) {
                                keys.push(key);
                            }
                        }

                        parameter=keys;
                        nilai=Object.values(result);

                        // for(var k=0; k<40; k++){
                            if(1==section[0]){
                                tempsec=section[0];
                                // console.log(weight);
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]){
                                            a=a+'<td>A</td>';
                                        }
                                    break;
                                }
                            }
                            else if(2==section[0]){
                                tempsec=section[0];
                                for(m=0;m<5;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]){
                                            a=a+'<td>A</td>';
                                        }
                                    break;
                                }
                            }
                            else if(3==section[0]){
                                tempsec=section[0];
                                for(m=0;m<12;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]){
                                            a=a+'<td>A</td>';
                                        }
                                    break;
                                }
                            }
                            else if(4==section[0]){
                                tempsec=section[0];

                                for(m=0;m<17;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 40:
                                        if(nilai[0]){
                                            a=a+'<td>A</td>';
                                        }


                                    break;


                                }
                            }
                            else if(5==section[0]){
                                tempsec=section[0];
                                for(m=0;m<21;m++){
                                    a=a+'<td>'+' '+'</td>';

                                }
                                a=a+'<td>A</td>';
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]<=22){
                                            a=a+'<td>A</td>';
                                        }


                                    break;


                                }
                            }
                            else if(6==section[0]){
                                tempsec=section[0];
                                for(m=0;m<27;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]){
                                            a=a+'<td>A</td>';
                                        }


                                    break;


                                }
                            }
                            else if(7==section[0]){
                                tempsec=section[0];
                                for(m=0;m<33;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 20:
                                        if(nilai[0]<=15){
                                            a=a+'<td>'+'A'+'</td>';
                                        }

                                        else if(nilai[0]>15 && nilai[0]<=20 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                        else if(nilai[0]>20 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }
                            }
total=nilai[0];
                            for(var k=1; k<nilai.length; k++){


//cek sama/beda dg yg sebelumnya
if(tempsec==section[k]){
    total=total+nilai[k];
    tempsec=section[k];
    //  weight=weight[k]*100;
    switch (weight[k]*100) {
        case 10:
            if(nilai[k]<=7){
                a=a+'<td>A</td>';
            }

            else if(nilai[k]>7 && nilai[k]<=10 ){
                a=a+'<td>A</td>';
            }
            else if(nilai[k]==10 ){
                a=a+'<td>A</td>';
            }
        break;
        case 15:
            if(nilai[k]<=8){
                a=a+'<td>A</td>';
            }

            else if(nilai[k]>8 && nilai[k]<=10 ){
                a=a+'<td>A</td>';
            }
            else if(nilai[k]>10 ){
                a=a+'<td>A</td>';
            }
        break;
        case 20:
            if(nilai[k]<=15){
                a=a+'<td>A</td>';
            }

            else if(nilai[k]>15 && nilai[k]<=20 ){
                a=a+'<td>A</td>';
            }
            else if(nilai[k]>20 ){
                a=a+'<td>A</td>';
            }
        break;
        case 30:
            if(nilai[k]<=20){
                a=a+'<td>A</td>';
            }

            else if(nilai[k]>20 && nilai[k]<=25 ){
                a=a+'<td>A</td>';
            }
            else if(nilai[k]>25 ){
                a=a+'<td>A</td>';
            }
        break;
           case 40:
            if(nilai[k]<=30){
                a=a+'<td>A</td>';
            }

            else if(nilai[k]>30 && nilai[k]<=35 ){
                a=a+'<td>A</td>';
            }
            else if(nilai[k]>35 ){
                a=a+'<td>A</td>';
            }
        break;

        default:
        a=a+'<td>A</td>';


    }
    //jika row terakhir
    if(k==(nilai.length-1)){
        //jika yang terakhir adalah 1,2 dst
        if(1==section[k]){
            for(m=0;m<35;m++){
                if(m==0){
                    if(total<=60){
                        a=a+'<td><b>A</td>';
                    }

                    else if(total>=61 && total<=79 ){
                        a=a+'<td><b>A</td>';
                    }
                    else if(total>79 ){
                        a=a+'<td><b>A</td>';
                    }

                }
                a=a+'<td>'+' '+'</td>';
            }
        }
        else if(2==section[k]){
            for(m=0;m<28;m++){
                if(m==0){
                    if(total<=60){
                        a=a+'<td><b>A</td>';
                    }

                    else if(total>=61 && total<=79 ){
                        a=a+'<td><b>A</td>';
                    }
                    else if(total>79 ){
                        a=a+'<td><b>A</td>';
                    }

                }
                a=a+'<td>'+' '+'</td>';
            }
        }
        else if(3==section[k]){
            for(m=0;m<23;m++){
                if(m==0){
                    if(total<=60){
                        a=a+'<td><b>A</td>';
                    }

                    else if(total>=61 && total<=79 ){
                        a=a+'<td><b>A</td>';
                    }
                    else if(total>79 ){
                        a=a+'<td><b>A</td>';
                    }

                }
                a=a+'<td>'+' '+'</td>';
            }
        }
        else if(4==section[k]){
            for(m=0;m<19;m++){
                if(m==0){
                    if(total<=60){
                        a=a+'<td><b>A</td>';
                    }

                    else if(total>=61 && total<=79 ){
                        a=a+'<td><b>A</td>';
                    }
                    else if(total>79 ){
                        a=a+'<td><b>A</td>';
                    }

                }
                a=a+'<td>'+' '+'</td>';
            }
        }
        else if(5==section[k]){
            for(m=0;m<13;m++){
                if(m==0){
                    if(total<=60){
                        a=a+'<td><b>A</td>';
                    }

                    else if(total>=61 && total<=79 ){
                        a=a+'<td><b>A</td>';
                    }
                    else if(total>79 ){
                        a=a+'<td><b>A</td>';
                    }

                }
                a=a+'<td>'+' '+'</td>';
            }
        }
        else if(6==section[k]){
            for(m=0;m<7;m++){
                if(m==0){
                    if(total<=60){
                        a=a+'<td><b>A</td>';
                    }

                    else if(total>=61 && total<=79 ){
                        a=a+'<td><b>A</td>';
                    }
                    else if(total>79 ){
                        a=a+'<td><b>A</td>';
                    }

                }
                a=a+'<td>'+' '+'</td>';
            }
        }
        else if(7==section[k]){
                                                m=0;
                                                // for(m=0;m<1;m++){
                                                if(m==0){
                                                    if(total<=60){
                                                        a=a+'<td style="background-color:yellow;"<b>'+'C'+'</td>';
                                                    }
                                                    else if(total>=61 && total<=79 ){
                                                        a=a+'<td><b>'+'B'+'</td>';
                                                    }
                                                    else if(total>79 ){
                                                        a=a+'<td><b>'+'A'+'</td>';
                                                    }
                                                    else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                    }
                                                }


                                            }



    }

}
//jika beda
else{
    var temptot=total;
    total=nilai[k];
    tempsec=section[k];
//total sebelumnya
    // a=a+'<td>'+temptot+'</td>';
    if(temptot<=60){
        a=a+'<td><b>C</td>';
    }

    else if(temptot>=61 && temptot<=79 ){
        a=a+'<td><b>B</td>';
    }
    else if(temptot>79 ){
        a=a+'<td><b>A</td>';
    }

//isi baru setelah yang pertama selesai / ketika beda
    if(2==section[k]){
        tempsec=section[k];
        if(section[k-1]==1){
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }
        // for(m=0;m<7;m++){
        //         a=a+'<td>'+' '+'</td>';
        // }

    }
    else if(3==section[k]){
        // console.log(section[k-1]);
        tempsec=section[k];

        if(section[k-1]==1){
            for(m=0;m<7;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }
        else if(section[k-1]==2){

            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }



    }
    else if(4==section[k]){
        tempsec=section[k];
        if(section[k-1]==1){
            for(m=0;m<12;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==2){
            for(m=0;m<5;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==3){

            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }

    }
    else if(5==section[k]){
        tempsec=section[k];
        if(section[k-1]==1){
            for(m=0;m<16;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }
        else if(section[k-1]==2){
            for(m=0;m<9;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==3){
            for(m=0;m<4;m++){
                a=a+'<td>A</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==4){

            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }

    }
    else if(6==section[k]){
        tempsec=section[k];
        if(section[k-1]==1){
            for(m=0;m<22;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==2){
            for(m=0;m<15;m++){
                a=a+'<td>A</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==3){
            for(m=0;m<10;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==4){
            for(m=0;m<6;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==5){

            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }

    }
    else if(7==section[k]){
        tempsec=section[k];

        if(section[k-1]==1){
            for(m=0;m<28;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }
        else if(section[k-1]==2){
            for(m=0;m<21;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }

        else if(section[k-1]==3){
            for(m=0;m<16;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }
        else if(section[k-1]==4){
            for(m=0;m<12;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;
                // default:
                // a=a+'<td>'+nilai[k]+'</td>';
            }
        }

        else if(section[k-1]==5){
            for(m=0;m<6;m++){
                a=a+'<td>'+' '+'</td>';
            }
            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
        else if(section[k-1]==6){

            switch (weight[k]*100) {
                case 10:
                if(nilai[k]<=7){
                    a=a+'<td>A</td>';
                }

                else if(nilai[k]>7 && nilai[k]<=10 ){
                    a=a+'<td>A</td>';
                }
                else if(nilai[k]==10 ){
                    a=a+'<td>A</td>';
                }
                break;
                case 15:
                    if(nilai[k]<=8){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>8 && nilai[k]<=10 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>10 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 20:
                    if(nilai[k]<=15){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>15 && nilai[k]<=20 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>20 ){
                        a=a+'<td>A</td>';
                    }
                break;

                case 30:
                    if(nilai[k]<=20){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>20 && nilai[k]<=25 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>25 ){
                        a=a+'<td>A</td>';
                    }
                break;
                case 40:
                    if(nilai[k]<=30){
                        a=a+'<td>A</td>';
                    }

                    else if(nilai[k]>30 && nilai[k]<=35 ){
                        a=a+'<td>A</td>';
                    }
                    else if(nilai[k]>35 ){
                        a=a+'<td>A</td>';
                    }
                break;

            }
        }
    }



}




}



                        // }
                        a=a+'</tr>';
                            return a;

                    }



                    $('#table-body').append('<tr>'+'<td>' + 'Actual' + '</td>'+actual(data[i].nilai,data[i].section,data[i].parameter,data[i].weight));

                    function actual(nilai, section, parameter, weight){
                        var a,b,c,total;
                        nilai=nilai.split(',');
                        section=section.split(',');
                        parameter=parameter.split(',');
                        weight=weight.split(',');

                        console.log(weight,nilai);

                        nilai = nilai.map(Number)

                        parameter = parameter.map(Number)

                        var result = {};
                        parameter.forEach((key, i) => result[key] = nilai[i]);


                        var keys = [];
                        var values = [];

                        for (var key in result) {
                            if (result.hasOwnProperty(key)) {
                                keys.push(key);
                            }
                        }

                        parameter=keys;
                        nilai=Object.values(result);





                        //cek start darimana
                            if(1==section[0]){
                                tempsec=section[0];
                                // console.log(weight);
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]<=20){
                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                        }

                                        else if(nilai[0]>20 && nilai[0]<=25 ){
                                            a=a+'<td>'+'B'+'</td>';
                                        }
                                        else if(nilai[0]>25 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }


                            }
                            else if(2==section[0]){
                                tempsec=section[0];
                                for(m=0;m<5;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]<=20){
                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                        }

                                        else if(nilai[0]>20 && nilai[0]<=25 ){
                                            a=a+'<td>'+'B'+'</td>';
                                        }
                                        else if(nilai[0]>25 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }
                            }
                            else if(3==section[0]){
                                tempsec=section[0];
                                for(m=0;m<12;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]<=20){
                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                        }

                                        else if(nilai[0]>20 && nilai[0]<=25 ){
                                            a=a+'<td>'+'B'+'</td>';
                                        }
                                        else if(nilai[0]>25 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }
                            }
                            else if(4==section[0]){
                                tempsec=section[0];

                                for(m=0;m<17;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 40:
                                        if(nilai[0]<=30){
                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                        }

                                        else if(nilai[0]>30 && nilai[0]<=35 ){
                                            a=a+'<td>'+'B'+'</td>';
                                        }
                                        else if(nilai[0]>35 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }
                            }
                            else if(5==section[0]){
                                tempsec=section[0];
                                for(m=0;m<21;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]<=20){
                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                        }

                                        else if(nilai[0]>20 && nilai[0]<=25 ){
                                            a=a+'<td>'+'B'+'</td>';
                                        }
                                        else if(nilai[0]>25 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }
                            }
                            else if(6==section[0]){
                                tempsec=section[0];
                                for(m=0;m<27;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 30:
                                        if(nilai[0]<=20){
                                            a=a+'<td>'+'C'+'</td>';
                                        }

                                        else if(nilai[0]>20 && nilai[0]<=25 ){
                                            a=a+'<td>'+'B'+'</td>';
                                        }
                                        else if(nilai[0]>25 ){
                                            a=a+'<td>'+'A'+'</td>';
                                        }
                                    break;


                                }
                            }
                            else if(7==section[0]){
                                tempsec=section[0];
                                for(m=0;m<33;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                weightt=weight[0]*100;
                                switch (weightt) {
                                    case 20:
                                        if(nilai[0]<=15){
                                            a=a+'<td>'+nilai[0]+'</td>';
                                        }

                                        else if(nilai[0]>15 && nilai[0]<=20 ){
                                            a=a+'<td>'+nilai[0]+'</td>';
                                        }
                                        else if(nilai[0]>20 ){
                                            a=a+'<td>'+nilai[0]+'</td>';
                                        }
                                    break;


                                }
                            }
                            total=nilai[0];
                            //mulai dari 1 karena 0 sudah dideclare sebelumnya
                            for(var k=1; k<nilai.length; k++){


                                    //cek sama/beda dg yg sebelumnya
                                    if(tempsec==section[k]){
                                        total=total+nilai[k];
                                        tempsec=section[k];
                                        //  weight=weight[k]*100;
                                        switch (weight[k]*100) {
                                            case 10:
                                                if(nilai[k]<=7){
                                                    a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                }

                                                else if(nilai[k]>7 && nilai[k]<=10 ){
                                                    a=a+'<td>'+'B'+'</td>';
                                                }
                                                else if(nilai[k]==10 ){
                                                    a=a+'<td>'+'A'+'</td>';
                                                }
                                            break;
                                            case 15:
                                                if(nilai[k]<=8){
                                                    a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                }

                                                else if(nilai[k]>8 && nilai[k]<=10 ){
                                                    a=a+'<td>'+'B'+'</td>';
                                                }
                                                else if(nilai[k]>10 ){
                                                    a=a+'<td>'+'A'+'</td>';
                                                }
                                            break;
                                            case 20:
                                                if(nilai[k]<=15){
                                                    a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                }

                                                else if(nilai[k]>15 && nilai[k]<=20 ){
                                                    a=a+'<td>'+'B'+'</td>';
                                                }
                                                else if(nilai[k]>20 ){
                                                    a=a+'<td>'+'A'+'</td>';
                                                }
                                            break;
                                            case 30:
                                                if(nilai[k]<=20){
                                                    a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                }

                                                else if(nilai[k]>20 && nilai[k]<=25 ){
                                                    a=a+'<td>'+'B'+'</td>';
                                                }
                                                else if(nilai[k]>25 ){
                                                    a=a+'<td>'+'A'+'</td>';
                                                }
                                            break;
                                               case 40:
                                                if(nilai[k]<=30){
                                                    a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                }

                                                else if(nilai[k]>30 && nilai[k]<=35 ){
                                                    a=a+'<td>'+'B'+'</td>';
                                                }
                                                else if(nilai[k]>35 ){
                                                    a=a+'<td>'+'A'+'</td>';
                                                }
                                            break;

                                            // default:
                                                // a=a+'<td>'+nilai[k]+'</td>';


                                        }
                                        //jika row terakhir
                                        if(k==(nilai.length-1)){
                                            z=nilai.length-1;
                                        // console.log('nilai length '+z);
                                            //jika yang terakhir adalah 1,2 dst
                                            if(1==section[k]){
                                                for(m=0;m<35;m++){
                                                    if(m==0){
                                                        if(total<=60){
                                                            a=a+'<td style="background-color:yellow;"><b>'+'C'+'</td>';
                                                        }

                                                        else if(total>=61 && total<=79 ){
                                                            a=a+'<td><b>'+'B'+'</td>';
                                                        }
                                                        else if(total>79 ){
                                                            a=a+'<td><b>'+'A'+'</td>';
                                                        }
                                                        else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                        }
                                                    }
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(2==section[k]){
                                                for(m=0;m<28;m++){
                                                    if(m==0){
                                                        if(total<=60){
                                                            a=a+'<td style="background-color:yellow;<b>">'+'C'+'</td>';
                                                        }

                                                        else if(total>=61 && total<=79 ){
                                                            a=a+'<td><b>'+'B'+'</td>';
                                                        }
                                                        else if(total>79 ){
                                                            a=a+'<td><b>'+'A'+'</td>';
                                                        }
                                                        else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                        }
                                                    }
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(3==section[k]){
                                                for(m=0;m<23;m++){
                                                    if(m==0){
                                                        if(total<=60){
                                                            a=a+'<td style="background-color:yellow;"><b>'+'C'+'</td>';
                                                        }

                                                        else if(total>=61 && total<=79 ){
                                                            a=a+'<td><b>'+'B'+'</td>';
                                                        }
                                                        else if(total>79 ){
                                                            a=a+'<td><b>'+'A'+'</td>';
                                                        }
                                                        else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                        }
                                                    }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }

                                            else if(4==section[k]){
                                                for(m=0;m<19;m++){
                                                    if(m==0){
                                                        if(total<=60){
                                                            a=a+'<td style="background-color:yellow;"><b>'+'C'+'</td>';
                                                        }

                                                        else if(total>=61 && total<=79 ){
                                                            a=a+'<td><b>'+'B'+'</td>';
                                                        }
                                                        else if(total>79 ){
                                                            a=a+'<td><b>'+'A'+'</td>';
                                                        }
                                                        else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                        }
                                                    }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(5==section[k]){
                                                for(m=0;m<13;m++){
                                                    if(m==0){
                                                        if(total<=60){
                                                            a=a+'<td style="background-color:yellow;<b>">'+'C'+'</td>';
                                                        }

                                                        else if(total>=61 && total<=79 ){
                                                            a=a+'<td><b>'+'B'+'</td>';
                                                        }
                                                        else if(total>79 ){
                                                            a=a+'<td><b>'+'A'+'</td>';
                                                        }
                                                        else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                        }
                                                    }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(6==section[k]){
                                                for(m=0;m<7;m++){
                                                    if(m==0){
                                                        if(total<=60){
                                                            a=a+'<td style="background-color:yellow;"<b>>'+'C'+'</td>';
                                                        }
                                                        else if(total>=61 && total<=79 ){
                                                            a=a+'<td><b>'+'B'+'</td>';
                                                        }
                                                        else if(total>79 ){
                                                            a=a+'<td><b>'+'A'+'</td>';
                                                        }
                                                        else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                        }
                                                    }
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(7==section[k]){
                                                m=0;
                                                // for(m=0;m<1;m++){
                                                if(m==0){
                                                    if(total<=60){
                                                        a=a+'<td style="background-color:yellow;"<b>'+'C'+'</td>';
                                                    }
                                                    else if(total>=61 && total<=79 ){
                                                        a=a+'<td><b>'+'B'+'</td>';
                                                    }
                                                    else if(total>79 ){
                                                        a=a+'<td><b>'+'A'+'</td>';
                                                    }
                                                    else{
                                                            a=a+'<td><b>'+total+'</td>';
                                                    }
                                                }


                                            }

                                        }

                                    }
                                    //jika beda
                                    else{
                                        var temptot=total;
                                        total=nilai[k];
                                        tempsec=section[k];
                                    //total sebelumnya
                                        // a=a+'<td>'+temptot+'</td>';
                                        if(temptot<=60){
                                            a=a+'<td style="background-color:yellow;"><b>'+'C'+'</td>';
                                        }

                                        else if(temptot>=61 && temptot<=79 ){
                                            a=a+'<td><b>'+'B'+'</td>';
                                        }
                                        else if(temptot>79 ){
                                            a=a+'<td><b>'+'A'+'</td>';
                                        }

                                    //isi baru setelah yang pertama selesai / ketika beda
                                        if(2==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }
                                            // for(m=0;m<7;m++){
                                            //         a=a+'<td>'+' '+'</td>';
                                            // }

                                        }
                                        else if(3==section[k]){
                                            // console.log(section[k-1]);
                                            tempsec=section[k];

                                            if(section[k-1]==1){
                                                for(m=0;m<7;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }
                                            else if(section[k-1]==2){

                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }



                                        }
                                        else if(4==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<12;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<5;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==3){

                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }

                                        }
                                        else if(5==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<16;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<9;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==3){
                                                for(m=0;m<4;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==4){

                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }

                                        }
                                        else if(6==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<22;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<15;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==3){
                                                for(m=0;m<10;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==4){
                                                for(m=0;m<6;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==5){

                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }

                                        }
                                        else if(7==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<28;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<21;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }

                                            else if(section[k-1]==3){
                                                for(m=0;m<16;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }
                                            else if(section[k-1]==4){
                                                for(m=0;m<12;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    // default:
                                                    // a=a+'<td>'+nilai[k]+'</td>';
                                                }
                                            }

                                            else if(section[k-1]==5){
                                                for(m=0;m<6;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }
                                            else if(section[k-1]==6){

                                                switch (weight[k]*100) {
                                                    case 10:
                                                    if(nilai[k]<=7){
                                                        a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                    }

                                                    else if(nilai[k]>7 && nilai[k]<=10 ){
                                                        a=a+'<td>'+'B'+'</td>';
                                                    }
                                                    else if(nilai[k]==10 ){
                                                        a=a+'<td>'+'A'+'</td>';
                                                    }
                                                    break;
                                                    case 15:
                                                        if(nilai[k]<=8){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>8 && nilai[k]<=10 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>10 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 20:
                                                        if(nilai[k]<=15){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>15 && nilai[k]<=20 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>20 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                    case 30:
                                                        if(nilai[k]<=20){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>20 && nilai[k]<=25 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>25 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;
                                                    case 40:
                                                        if(nilai[k]<=30){
                                                            a=a+'<td style="background-color:yellow;">'+'C'+'</td>';
                                                        }

                                                        else if(nilai[k]>30 && nilai[k]<=35 ){
                                                            a=a+'<td>'+'B'+'</td>';
                                                        }
                                                        else if(nilai[k]>35 ){
                                                            a=a+'<td>'+'A'+'</td>';
                                                        }
                                                    break;

                                                }
                                            }

                                        }



                                    }




                            }

                            a=a+'</tr>';
                        return a;

                    }

                    $('#table-body').append('<tr>'+'<td>' + 'Last review' + '</td>'+lastReview(data[i].section,data[i].created_at));
                    tempNama= data[i].nama;

                    function lastReview(section,tanggal){
                        var a,temptot;
                        tanggal=tanggal.split(',');
                        section=section.split(',');
                        //cek start darimana
                        if(1==section[0]){
                                tempsec=section[0];
                                // console.log(weight);
                                a=a+'<td>'+tanggal[0]+'</td>';


                            }
                            else if(2==section[0]){
                                tempsec=section[0];
                                for(m=0;m<5;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                a=a+'<td>'+tanggal[0]+'</td>';
                            }
                            else if(3==section[0]){
                                tempsec=section[0];
                                for(m=0;m<12;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                a=a+'<td>'+tanggal[0]+'</td>';
                            }
                            else if(4==section[0]){
                                tempsec=section[0];
                                // a=a+'<td>'+' '+'</td>';
                                // a=a+'<td>'+' '+'</td>';
                                for(m=0;m<17;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                a=a+'<td><b>'+tanggal[0]+'</td>';
                            }
                            else if(5==section[0]){
                                tempsec=section[0];
                                for(m=0;m<21;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                a=a+'<td>'+tanggal[0]+'</td>';
                            }
                            else if(6==section[0]){
                                tempsec=section[0];
                                for(m=0;m<27;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                a=a+'<td>'+tanggal[0]+'</td>';
                            }
                            else if(7==section[0]){
                                tempsec=section[0];
                                for(m=0;m<33;m++){
                                    a=a+'<td>'+' '+'</td>';
                                }
                                a=a+'<td>'+tanggal[0]+'</td>';
                            }
                        console.log("batas "+tanggal.length);
                        for(var k=1; k<tanggal.length; k++){
                               //cek sama/beda dg yg sebelumnya
                               if(tempsec==section[k]){
                                console.log("nilai k-nya "+k);
                                        tempsec=section[k];
                                        a=a+'<td>'+tanggal[k]+'</td>';

                                        //jika row terakhir
                                        if(k==(tanggal.length-1)){
                                            //jika yang terakhir adalah 1,2 dst
                                            if(1==section[k]){
                                                for(m=0;m<35;m++){
                                                    if(m==0){
                                                        a=a+'<td><b>'+tanggal[k]+'</td>';
                                                    }
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(2==section[k]){
                                                for(m=0;m<28;m++){
                                                if(m==0){
                                                    a=a+'<td><b>'+tanggal[k]+'</td>';
                                                }
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(3==section[k]){
                                                for(m=0;m<23;m++){
                                                if(m==0){
                                                    a=a+'<td><b>'+tanggal[k]+'</td>';
                                                }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(4==section[k]){
                                                for(m=0;m<19;m++){
                                                    if(m==0){
                                                        a=a+'<td><b>'+tanggal[k]+'</td>';
                                                    }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(5==section[k]){
                                                for(m=0;m<13;m++){
                                                if(m==0){
                                                    a=a+'<td><b>'+tanggal[k]+'</td>';
                                                }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(6==section[k]){
                                                for(m=0;m<7;m++){
                                                if(m==0){
                                                    a=a+'<td><b>'+tanggal[k]+'</td>';
                                                }

                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                            }
                                            else if(7==section[k]){
                                            m=0;
                                                if(m==0){
                                                    if(total<=60){
                                                        a=a+'<td ><b>'+tanggal[k]+'</td>';
                                                    }

                                                    else if(total>=61 && total<=79 ){
                                                        a=a+'<td><b>'+tanggal[k]+'</td>';
                                                    }
                                                    else if(total>79 ){
                                                        a=a+'<td><b>'+tanggal[k]+'</td>';
                                                    }

                                                }
                                                // a=a+'<td>'+' '+'</td>';

                                            }

                                        }

                                    }
                                    //jika beda
                                    else{

                                        tempsec=section[k];
                                    //total sebelumnya
                                        // a=a+'<td>'+temptot+'</td>';
                                        a=a+'<td ><b>'+tanggal[k]+'</td>';

                                    //isi baru setelah yang pertama selesai / ketika beda
                                        if(2==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }


                                        }
                                        else if(3==section[k]){
                                            // console.log(section[k-1]);
                                            tempsec=section[k];

                                            if(section[k-1]==1){
                                                for(m=0;m<7;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==2){

                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }



                                        }
                                        else if(4==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<12;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<5;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==3){

                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }

                                        }
                                        else if(5==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<16;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<9;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==3){
                                                for(m=0;m<4;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==4){

                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }

                                        }
                                        else if(6==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<22;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<15;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==3){
                                                for(m=0;m<10;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==4){
                                                for(m=0;m<6;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==5){

                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }

                                        }
                                        else if(7==section[k]){
                                            tempsec=section[k];
                                            if(section[k-1]==1){
                                                for(m=0;m<28;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==2){
                                                for(m=0;m<21;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }

                                            else if(section[k-1]==3){
                                                for(m=0;m<16;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==4){
                                                for(m=0;m<12;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }

                                            else if(section[k-1]==5){
                                                for(m=0;m<6;m++){
                                                    a=a+'<td>'+' '+'</td>';
                                                }
                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                            else if(section[k-1]==6){

                                                a=a+'<td>'+tanggal[k]+'</td>';
                                            }
                                        }



                                    }
                        }
                            a=a+'</tr>'
                            return a;

                    }


                }


	        },
	        error: function(response) {
                console.log(response);
	        }
	    });

        var table= '#example'
        $('#maxRows').on('change', function(){
            $('.pagination').html('')
            var trnum=0;
            var maxRows=parseInt($(this).val())
            var totalRows=$(table+' tbody tr').length
            $(table+' tr:gt(2)').each(function(){
                trnum++
                if(trnum > maxRows){
                    $(this).hide()
                }
                if(trnum <= maxRows){
                    $(this).show()
                }
            })
            if(totalRows > maxRows){
                var pagenum = Math.ceil(totalRows/maxRows)
                for(var i=1;i<=pagenum;){
                    $('.pagination').append('<li data-page="'+i+'">\<span>'+ i++ +'<span class="sr-only">(current)</span></span>\</li>').show()
                }
            }
            $('.pagination li:first-child').addClass('active')
            $('.pagination li').on('click', function(){
                var pageNum = $(this).attr('data-page')
                var trIndex = 0;
                $('.pagination li').removeClass('active')
                $(this).addClass('active')
                $(table+' tr:gt(2)').each(function(){
                    trIndex++
                    if(trIndex > (maxRows*pageNum) || trIndex <= ((maxRows*pageNum)-maxRows)){
                        $(this).hide()
                    }
                    else{
                        $(this).show()
                    }
                })
            })
        })

});


</script>
@endsection

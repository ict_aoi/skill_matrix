@extends('layouts.app', ['active' => 'report'])
@section('header')
    <div class="page-header page-header-default">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="#"><i class="icon-home2 position-left"></i> Input</a></li>
                <li class="active">Report</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="content">
        <div class="row">
            <div class=" panel panel-flat">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group " id="SectionDiv">
                            <label for="factory" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                                Masukan NIK / Nama
                            </label>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <select class="form-control select-search select2-hidden-accessible select_nik"
                                    id="select_nik" name="nik[]" tabindex="-1" aria-hidden="true">

                                </select>

                            </div>

                        </div>

                    </div>
                    <div class="row">
                        <span>


                            <div class="col-lg-6">

                                <input type="text" name="reportfrom" id="reportfrom" class="form-control"
                                    placeholder="Dari tanggal" />
                            </div>

                            <div class="col-lg-6">

                                <input type="text" name="reportto" id="reportto" class="form-control"
                                    placeholder="Sampai tanggal" />
                            </div>
                        </span>
                    </div>
                    <div class="row">
                        <div class="form-group " id="SectionDiv">

                            <button type="submit" id="export" class="btn btn-default col-xs-12">Export All <i
                                    class="icon-file-excel position-left"></i></button>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <a id="export_xls" href="{{ Route('input.getExcel') }}">

    @endsection

    @section('js')
        <script type="text/javascript">
            $(document).ready(function() {
                $('.select_section').select2();


                $(function() {
                    $('#reportfrom').datepicker();
                    $('#reportto').datepicker();
                });

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    type: 'get',
                    url: "{{ route('input.NIK') }}",

                    success: function(data) {

                        $('#select_nik').empty();
                        $('#select_nik').append('<option value=' + 0 + '>' + 'Silakan Pilih NIK / Nama' +
                            '</option>');
                        for (var i = 0; i < data.length; i++) {

                            $('#select_nik').append('<option value=' + data[i].nik + '>' + data[i].nik +
                                ' - ' + data[i].name + '</option>');

                        }


                    },
                    error: function(response) {
                        console.log('tidakoke');
                    }
                });


                $('#export').click(function() {
                    $('#reportrange').show();
                    $('#reportfrom').show();
                    $('#reportto').show();
                    var selectedNIK = $('.select_nik').select2("data");
                    var nik = "";
                    // var section = "";
                    var from = $('#reportfrom').val();
                    var to = $('#reportto').val();

                    for (var i = 0; i < selectedNIK.length; i++) {
                        var str = selectedNIK[i].id + ";";
                        nik = nik + str;
                    }

                    console.log(from);
                    console.log(to);
                    window.location.href = $('#export_xls').attr('href') + '?nik=' + nik + '&from=' + from;


                });

            });
        </script>
    @endsection

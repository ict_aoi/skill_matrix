@extends('layouts.app', ['active' => 'history'])
@section('header')
    <div class="page-header page-header-default">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="#"><i class="icon-home2 position-left"></i> Input</a></li>
                <li class="active">History</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="content">
        <div class="row">

            <div class=" panel panel-flat">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group " id="SectionDiv">

                            <div class="row">

                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <select class="form-control select-search select2-hidden-accessible select_nik"
                                        id="select_nik" name="nik[]" tabindex="-1" aria-hidden="true">

                                    </select>

                                </div>

                            </div>

                        </div>
                    </div>


                </div>
                <div class="panel-body">

                    <div class="row">
                        <h1 id="historyheader">History Penilaian</h1>

                        <div class="row" id=dataskill>
                            <div class="col-lg-6">

                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>NIK</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="nikk" readonly>

                                </div>
                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>Nama</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="nama" readonly>

                                </div>
                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>Departemen</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="department" readonly>

                                </div>
                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>Subdept</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="subdept" readonly>

                                </div>
                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>Position</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="position" readonly>

                                </div>
                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>Factory</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="factory" readonly>

                                </div>
                                <div class=" col-lg-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text "><b>Masa Kerja</b></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="masakerja" readonly>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <form id="form-foto" method="POST" enctype="multipart/form-data" action="/upload/proses">
                                    {{ csrf_field() }}
                                    <div class=" col-lg-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text "><b>Foto</b></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        {{-- <div class="input-group-text " id="foto"></div> --}}
                                        {{-- <input type="text" class="form-control" id="department" readonly> --}}

                                        {{-- Input Foto --}}
                                        <input type="file" name="foto" class="form-control">
                                    </div>

                                    <div class="row">

                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Sertifikat</b></div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="input-group-text " id="tampilsertifikat"></div>


                                        </div>
                                    </div>


                                </form>




                            </div>


                            {{-- </div> --}}
                        </div>
                        <div class="row">
                            <div class="table-responsive">
                                <table id="table" class="table table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Section</th>
                                            <th width="280px">Nilai</th>
                                            <th>Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="Modalinput" class="modal fade bd-example-modal-lg" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="modelHeading">Detail Nilai</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="table-responsive">
                                            <table id="table_modal" class="table table-bordered data-table-modal">
                                                <thead>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>Tanggal</th>
                                                        <th>Parameter</th>

                                                        <th>Nilai</th>
                                                        <th>Standard</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="table-body">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-sm-offset-2 col-sm-10">

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>

                </div>
            </div>

        @endsection

        @section('js')
            <script type="text/javascript">
                $(document).ready(function() {

                    $('.select_nik').select2();
                    // $('#tampilsertifikat').select2();

                    $('#dataskill').hide();
                    $('#table').hide();
                    $('#historyheader').hide();
                    // var nik= $('#nikkk').val();

                    $.ajax({
                        type: 'get',
                        url: "{{ route('input.NIK') }}",

                        success: function(data) {

                            $('#select_nik').empty();
                            $('#select_nik').append('<option value=' + 0 + '>' + 'Silakan Pilih NIK / Nama' +
                                '</option>');
                            for (var i = 0; i < data.length; i++) {

                                $('#select_nik').append('<option value=' + data[i].nik + '>' + data[i].nik +
                                    ' - ' + data[i].name + '</option>');
                            }


                        },
                        error: function(response) {
                            console.log('tidakoke');
                        }
                    });

                    $('#select_nik').on('change', function(e) {
                        $('#historyheader').show();
                        var selectedNIK = $('.select_nik').select2("data");
                        var nik = selectedNIK[0].id;
                        $('#dataskill').show();
                        $('#table').show();
                        $('#table').DataTable().destroy();
                        $('#table tbody').empty();


                        console.log(nik);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: 'post',
                            url: "{{ route('input.tampilfoto') }}",
                            data: {
                                nik: nik
                            },
                            success: function(data) {

                                // console.log(data);
                                $('#foto').empty();

                                var img = document.createElement("img");
                                if (data[0].file) {
                                    data = data[0].file;
                                    esrc = "/data_file/";
                                    esrc = esrc.concat(data);
                                } else {
                                    data = data;
                                    esrc = "/data_file/";
                                    esrc = esrc.concat(data);
                                }


                                // console.log(esrc);
                                img.src = esrc;
                                var src = document.getElementById("foto");
                                img.style.width = "150px";
                                src.appendChild(img);


                            },
                            error: function(response) {
                                console.log('tidakoke');
                            }
                        });
                        // document.getElementById("tampilsertifikat").value = "1";

                        $.ajax({
                            type: 'post',
                            url: "{{ route('input.tampilsertifikat') }}",
                            data: {
                                nik: nik
                            },
                            success: function(data) {
                                if (data == 'Belum Upload Sertifikat') {
                                    $('#tampilsertifikat').empty();
                                    $('#tampilsertifikat').append('<div class="input-group-text ">' +
                                        data + '</div>');
                                } else {

                                    fruits = [];
                                    $('#tampilsertifikat').val();
                                    $('#tampilsertifikat').empty()
                                    for (var i = 0; i < data.length; i++) {
                                        console.log(data[i].keterangan);
                                        fruits.push(data[i].keterangan);
                                        // $('#tampilsertifikat').val(data[i].keterangan);
                                        // document.getElementById("tampilsertifikat").value = "1";
                                        // $('#tampilsertifikat').append('<option disabled value=' + data[i].keterangan + '>' + data[i].keterangan + '</option>');
                                    }
                                    for (var i = 0; i < data.length; i++) {
                                        // $('#tampilsertifikat').val(fruits);
                                        $('#tampilsertifikat').append(
                                            '<input style="margin:5px;border-radius: 10%;" class="btn btn-primary btn-sm" type="button" value="' +
                                            data[i].keterangan + '">');

                                    }
                                }
                                // var implode = fruits.join(",");
                                // console.log(implode);
                                // $('#tampilsertifikat').val(implode);

                                //   $('#tampilsertifikat').val();
                                //     console.log(oke);


                            },
                            error: function(response) {
                                console.log('tidakoke');
                            }
                        });
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('input.getDataQC') }}",
                            data: {
                                nik: nik
                            },

                            success: function(data) {

                                $('#nikk').val(data[0].nik);
                                $('#nikkk').val(data[0].nik);
                                $('#nikkkk').val(data[0].nik);
                                $('#nama').val(data[0].name);
                                $('#department').val(data[0].department_name);
                                $('#subdept').val(data[0].subdept_name);
                                $('#position').val(data[0].position);
                                $('#factory').val(data[0].factory);
                                //tahun
                                var masakerja = data[0].masakerja;
                                var masakerjatahun = '20'.concat(masakerja.substring(0, 2));
                                var today = new Date();
                                var yearnow = today.getFullYear();
                                masakerjatahun = parseInt(masakerjatahun)
                                var berapatahun = (yearnow - masakerjatahun);


                                //bulan
                                var masakerjabulan = masakerja.substring(2, 4);
                                if (masakerjabulan.substring(0, 1) == 0) {
                                    masakerjabulan = masakerjabulan.substring(1, 4);
                                }
                                var today = new Date();
                                var monthnow = today.getMonth() + 1;
                                masakerjabulan = parseInt(masakerjabulan)


                                var berapabulan = (12 + (monthnow - masakerjabulan));
                                if (berapabulan == 12) {
                                    berapatahun = berapatahun + 1;
                                    berapabulan = 0;
                                }
                                berapatahun = berapatahun.toString().concat(' tahun')
                                berapabulan = berapabulan.toString().concat(' bulan')

                                var masakerjatotal = berapatahun.concat(' ', berapabulan);


                                $('#masakerja').val(masakerjatotal);
                                setTbSkill(nik);
                            },
                            error: function(response) {
                                console.log(response);
                            }
                        });
                    });

                    $('body').on('click', '.cekdetail', function() {
                        var id_skill = $(this).data('id');
                        var selectedNIK = $('.select_nik').select2("data");
                        var nik = selectedNIK[0].id;
                        console.log(nik);
                        $('#Modalinput').modal('show');
                        setTbdetail(id_skill, nik);
                    });

                    $("#Modalinput").on('hidden.bs.modal', function() {
                        $('#table_modal').DataTable().destroy();
                        $('#tabel_modal tbody').empty();
                    });




                });

                function setTbSkill(nik) {

                    $.extend($.fn.dataTable.defaults, {
                        stateSave: true,
                        autoWidth: false,
                        autoLength: false,
                        processing: true,
                        serverSide: true,
                        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                        language: {
                            search: '<span>Filter:</span> _INPUT_',
                            searchPlaceholder: 'Type to filter...',
                            lengthMenu: '<span>Show:</span> _MENU_',
                            paginate: {
                                'first': 'First',
                                'last': 'Last',
                                'next': '&rarr;',
                                'previous': '&larr;'
                            }
                        }
                    });

                    var table = $('#table').DataTable({

                        ajax: {
                            type: 'GET',
                            url: "{{ route('input.getSkill') }}",
                            data: function(d) {
                                return $.extend({}, d, {
                                    "nik": nik
                                });
                            }
                        },

                        columns: [{
                                data: null,
                                sortable: false,
                                visible: false,
                                orderable: false,
                                searchable: false
                            },
                            {
                                data: 'idsection',
                                name: 'idsection'
                            },
                            {
                                data: 'total',
                                name: 'total',
                                searchable: false
                            },
                            {
                                data: 'created_at',
                                name: 'created_at'
                            },
                            {
                                data: 'code',
                                name: 'code',
                                searchable: false
                            },

                        ],
                        rowCallback: function(row, data, index) {

                            if (data.total == 'A' || data.total == 'B') {
                                $(row).css('background-color', '#cbf2aa');
                            } else {
                                $(row).css('background-color', '#f2aaaa');
                            }
                        }
                    });


                }

                function setTbdetail(id_skill, nik) {

                    var table_detail = $('#table_modal').DataTable({

                        ajax: {
                            type: 'GET',
                            url: "{{ route('input.getSkillDetail') }}",
                            data: function(d) {
                                return $.extend({}, d, {
                                    "id_skill": id_skill,
                                    "nik": nik
                                });
                            }

                        },

                        columns: [{
                                data: null,
                                sortable: false,
                                visible: false,
                                orderable: false,
                                searchable: false
                            },
                            {
                                data: 'created_at',
                                name: 'created_at'
                            },
                            {
                                data: 'idparameter',
                                name: 'idparameter'
                            },
                            {
                                data: 'nilai',
                                name: 'nilai'
                            },
                            {
                                data: 'standar',
                                name: 'standar'
                            },

                        ],
                        rowCallback: function(row, data, index) {

                            if (data.nilai.substring(0, 1) == 'A') {
                                $(row).css('background-color', '#cbf2aa');
                            } else {
                                $(row).css('background-color', '#f2aaaa');
                            }
                        }
                    });


                }
            </script>
        @endsection

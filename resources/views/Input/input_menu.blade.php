@extends('layouts.app', ['active' => 'input'])
@section('header')
    <div class="page-header page-header-default">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="#"><i class="icon-home2 position-left"></i> Input</a></li>
                <li class="active">Input Matrix</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="content">
        <div class="row">

            {{-- Panel NIK --}}
            <div class=" panel panel-flat">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group " id="SectionDiv">

                            <h1>Masukan NIK / Nama</h1>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <select class="form-control select-search select2-hidden-accessible select_nik"
                                    id="select_nik" name="nik[]" tabindex="-1" aria-hidden="true">

                                </select>

                            </div>

                        </div>
                    </div>

                </div>

            </div>

            {{-- Panel data --}}
            <div class="panel panel-flat">
                <div class="panel-body">

                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#inputPenilaianPane" data-toggle="tab">Input Penilaian</a>
                        </li>
                        <li><a href="#historyPane" data-toggle="tab">History</a>
                        </li>
                        <li><a href="#reportPane" data-toggle="tab">Report</a>
                        </li>
                    </ul>

                    <div class="tab-content ">

                        {{-- Input Penilaian Tab --}}
                        <div class="tab-pane active" id="inputPenilaianPane">

                            <div class="container-fluid">

                                <h1>Data Karyawan</h1>

                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>NIK</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="nikk" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>Nama</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="name" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>Department</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="department" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>Sub Department</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="subdept" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>Position</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="position" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>Factory</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="factory" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p><b>Masa Kerja</b></p>
                                        </div>

                                        <div class="col-md-9">
                                            <input type="text" id="masakerja" class="form-control" readonly>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <form action="/upload/proses" enctype="multipart/form-data" method="post"
                                        id="form-foto">

                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-3">
                                                <p><b>Foto</b></p>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="input-group-text " id="foto"></div>
                                                {{-- <input type="text" class="form-control" id="department" readonly> --}}
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class=" col-lg-3">
                                                <div class="input-group-prepend">
                                                    {{-- <div class="input-group-text "><b>Ganti Foto</b></div> --}}
                                                    {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
                                                </div>
                                            </div>

                                            <div class="col-md-9">
                                                <input type="file" name="filee">

                                                <span class="help-block text-danger">*ekstensi gambar harus
                                                    jpeg,jpg
                                                    dengan size max 5 mb</span>
                                                {{-- <input type="text" class="form-control" id="nikkk" hidden> --}}

                                                <div class="form-group">
                                                    <b hidden>NIK yg dikirim</b>
                                                    <textarea class="form-control hidden" id="nikkkk" name="nik"></textarea>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class=" col-md-3">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text "><b>Sertifikat</b></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="input-group-text " id="tampilsertifikat">

                                                </div>
                                            </div>
                                        </div>

                                        <div id="inputFileSertif">

                                            <div class="row">
                                                <button type="button" data-toggle="collapse" data-target="#addSertifikat" class="btn btn-link">
                                                    Tambah Sertifikat <i class="fa fa-chevron-down"></i>
                                                </button>

                                                <div id="addSertifikat" class="collapse">
                                                    <div class="row">
                                                        <div class=" col-lg-3">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text "><b>Judul Sertifikat</b></div>
                                                                {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">

                                                            <div class="form-group">

                                                                <textarea class="form-control" id="keterangan"
                                                                    name="keterangan"></textarea>
                                                                {{-- <span class="help-block text-danger">*masing-masing judul sertifikat dipisahkan oleh huruf "," (koma)</span>. --}}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class=" col-md-3">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text hidden"><b>Tambahkan</b></div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="file" name="file" placeholder="Choose File" id="file"
                                                                size="40" class="form-control">
                                                            <span class="help-block text-danger">*ekstensi gambar harus
                                                                jpeg,jpg
                                                                dengan size max 5 mb tiap sertifikat<br>*maximal jumlahnya 5</span>.
                                                            {{-- <span class="help-block text-danger">*maximal jumlahnya 5</span> --}}

                                                            {{-- <div class="form-group">
                                                                <b hidden>NIK yg dikirim</b>
                                                                <textarea class="form-control hidden" id="nikkkk" name="nik" ></textarea>

                                                            </div> --}}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                        {{-- Modal Detail --}}
                                        <div id="modalSertif" class="modal fade bd-example-modal-lg" role="dialog">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="modelHeading">Sertifikat</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <h3>Judul Sertifikat :</h3>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="modalJudulSertif" readonly>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <h3>Sertifikat :</h3>
                                                            </div>
                                                            <div class="col-md-4" id="modalFotoSertifikat">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <button class="btn btn-success" type="submit">Upload</button>
                                        </div>

                                    </form>
                                </div>

                            </div>

                            <div class="row">

                                <div class="form-group " id="SectionDiv">
                                    <h1>
                                        Pilih Section
                                    </h1>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <select class="form-control select-search select2-hidden-accessible"
                                            id="select_section" name="section" tabindex="-1" aria-hidden="true">

                                        </select>
                                        <span class="SectionReq help-block text-danger">*Harap isi NIK terlebih
                                            dahulu</span>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <h1 id="header_param">Input Paramater</h1>
                                <div class="col-lg-12">

                                    <div class="row">
                                        <form id="formParam">
                                            <div class="form-group" id="Parameter"></div>
                                        </form>


                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-lg-2 "></div>
                                    <div class="col-lg-2"></div>
                                    <button id="SavePage" name="SavePage" class="btn btn-primary col-lg-4">Save <i
                                            class="icon-floppy-disk"></i></button>
                                    <div class="col-lg-2"></div>

                                </div>
                            </div>
                        </div>

                        {{-- History Tab --}}
                        <div class="tab-pane" id="historyPane">
                            <div class="row">
                                <h1 id="historyheader">History Penilaian</h1>

                                {{-- Tabel --}}
                                <div class="row">
                                    <div class="table-responsive">
                                        <table id="table" class="table table-bordered data-table">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Section</th>
                                                    <th width="280px">Nilai</th>
                                                    <th>Tanggal</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                {{-- Modal Detail --}}
                                <div id="Modalinput" class="modal fade bd-example-modal-lg" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="modelHeading">Detail Nilai</h4>
                                            </div>
                                            <div class="modal-body">

                                                <div class="table-responsive">
                                                    <table id="table_modal" class="table table-bordered data-table-modal">
                                                        <thead>
                                                            <tr>
                                                                <th>id</th>
                                                                <th>Tanggal</th>
                                                                <th>Parameter</th>

                                                                <th>Nilai</th>
                                                                <th>Standard</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="table-body">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-sm-offset-2 col-sm-10">

                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>

                        {{-- Report Tab --}}
                        <div class="tab-pane" id="reportPane">
                            <div class="row">
                                <span>


                                    <div class="col-lg-6">

                                        <input type="text" name="reportfrom" id="reportfrom" class="form-control"
                                            placeholder="Dari tanggal" />
                                    </div>

                                    <div class="col-lg-6">

                                        <input type="text" name="reportto" id="reportto" class="form-control"
                                            placeholder="Sampai tanggal" />
                                    </div>
                                </span>
                            </div>
                            <div class="row">
                                <div class="form-group " id="SectionDiv">

                                    <button id="showExcel" class="btn btn-default col-xs-12">Tampilkan dulu</button>

                                </div>
                            </div>

                            <div class="row">

                                <div class="form-group " id="SectionDiv">

                                    <button type="submit" id="export" class="btn btn-default col-xs-12">Ekspor Semua<i
                                            class="icon-file-excel position-left"></i></button>

                                </div>
                            </div>


                            <div class="row">
                                <div class="table-responsive">
                                    <table id="table-report-preview" class="table table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>Tanggal Buat</th>
                                                {{-- <th>NIK</th>
                                                <th>Nama</th> --}}
                                                <th>Nilai</th>
                                                <th>Bobot</th>
                                                <th>Huruf</th>
                                                <th>Standar</th>
                                                <th>Section</th>
                                                <th>Parameter</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    </div>
    <a id="export_xls" href="{{ Route('input.getExcel') }}">
    @endsection

    @section('js')
        <script type="text/javascript">
            $(document).ready(function() {

                $('.select_nik').select2();

                $("#header_param").hide();

                $("#headerdash").hide();
                $(".SectionReq").show();

                // Ajax Setup
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                // Input NIK
                $.ajax({
                    type: 'get',
                    url: "{{ route('input.NIK') }}",

                    success: function(data) {

                        $('#select_nik').empty();
                        $('#select_nik').append('<option value=' + 0 + '>' + 'Silakan Pilih NIK / Nama' +
                            '</option>');
                        for (var i = 0; i < data.length; i++) {

                            $('#select_nik').append('<option value=' + data[i].nik + '>' + data[i].nik +
                                ' - ' + data[i].name + '</option>');

                        }


                    },
                    error: function(response) {
                        console.log('tidakoke');
                    }
                });

                // Data Table
                var table = $('#table-list').DataTable({
                    processing: true,
                    serverSide: true,
                    deferRender: true,
                    dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        searchPlaceholder: 'Type to filter...',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {
                            'first': 'First',
                            'last': 'Last',
                            'next': '&rarr;',
                            'previous': '&larr;'
                        }
                    },
                    ajax: {
                        type: 'GET',
                        url: "{{ route('master.getDataMachine') }}"
                    },
                    fnCreatedRow: function(row, data, index) {
                        var info = table.page.info();
                        var value = index + 1 + info.start;
                        $('td', row).eq(0).html(value);
                    },
                    columns: [{
                            data: null,
                            sortable: false,
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'machine_name',
                            name: 'machine_name'
                        },
                        {
                            data: 'machine_group',
                            name: 'machine_group'
                        },
                        {
                            data: 'machine_category',
                            name: 'machine_category'
                        }
                    ]
                });

                // ON change nik
                $('#select_nik').on('change', function(e) {
                    // Input Penilaian
                    // $('#Modalinput').modal('show');
                    var nik = $('#select_nik').val();
                    console.log(nik);

                    // History
                    $('#dataskill').show();
                    $('#table').show();
                    $('#table').DataTable().destroy();
                    $('#table tbody').empty();

                    // Fetch All Data
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('input.getInputData') }}",
                        data: {
                            nik: nik
                        },
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner9 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(data) {
                            console.log('Success : ', data);

                            // Data Absensi
                                $('#nikk').val(data[0].nik);
                                $('#nikkkk').val(data[0].nik);
                                $('#name').val(data[0].name);
                                $('#department').val(data[0].department_name);
                                $('#subdept').val(data[0].subdept_name);
                                $('#position').val(data[0].position);
                                $('#factory').val(data[0].factory);

                                // Hitung masa kerja
                                //tahun
                                var masakerja = data[0].nik;
                                var masakerjatahun = '20'.concat(masakerja.substring(0, 2));
                                var today = new Date();
                                var yearnow = today.getFullYear();
                                masakerjatahun = parseInt(masakerjatahun)
                                var berapatahun = (yearnow - masakerjatahun);

                                //bulan
                                var masakerjabulan = masakerja.substring(2, 4);
                                if (masakerjabulan.substring(0, 1) == 0) {
                                    masakerjabulan = masakerjabulan.substring(1, 4);
                                }
                                var today = new Date();
                                var monthnow = today.getMonth() + 1;
                                masakerjabulan = parseInt(masakerjabulan)

                                var berapabulan = (12 + (monthnow - masakerjabulan));
                                if (berapabulan >= 12) {
                                    berapatahun = berapatahun + 1;
                                    berapabulan = berapabulan-12;
                                }

                                berapatahun = berapatahun.toString().concat(' tahun')
                                berapabulan = berapabulan.toString().concat(' bulan')
                                var masakerjatotal = berapatahun.concat(' ', berapabulan);

                                $('#masakerja').val(masakerjatotal);
                                setTbSkill(data[0].nik);


                                $.ajax({
                                    type: 'post',
                                    url: "{{ route('input.tampilfoto') }}",
                                    data: {
                                        nik: nik
                                    },
                                    success: function(data) {

                                        console.log(data[0]);
                                        $('#foto').empty();

                                        var img = document.createElement("img");
                                        if (data[0].file) {
                                            data = data[0].file;
                                            esrc = "/data_file/";
                                            esrc = esrc.concat(data);
                                            // console.log(esrc);
                                        } else {
                                            data = data;
                                            esrc = "/data_file/";
                                            esrc = esrc.concat(data);

                                            // console.log(esrc);
                                        }


                                        // console.log(esrc);
                                        img.src = esrc;
                                        var src = document.getElementById("foto");
                                        img.style.width = "150px";
                                        src.appendChild(img);


                                    },
                                    error: function(response) {
                                        console.log('tidakoke');
                                    }
                                });


                            // Data Sertif
                                if (data[2] == 'Belum Upload Sertifikat') {
                                    $('#tampilsertifikat').empty();
                                    $('#tampilsertifikat').append('<div class="input-group-text ">' +
                                        data[2] + '</div>');
                                } else {

                                    fruits = [];
                                    $('#tampilsertifikat').val();
                                    $('#tampilsertifikat').empty()
                                    for (var i = 0; i < data[2].length; i++) {
                                        console.log(data[2][i].keterangan);
                                        fruits.push(data[2][i].keterangan);
                                        // $('#tampilsertifikat').val(data[2][i].keterangan);
                                        // document.getElementById("tampilsertifikat").value = "1";
                                        // $('#tampilsertifikat').append('<option disabled value=' + data[2][i].keterangan + '>' + data[2][i].keterangan + '</option>');
                                    }
                                    for (var i = 0; i < data[2].length; i++) {
                                        // $('#tampilsertifikat').val(fruits);
                                        $('#tampilsertifikat').append(
                                            '<div class="tes"><a class="buttonDetailSertifikat" data-id="' + data[2][i]
                                            .id +'">'+data[2][i].keterangan+'</a>' +'<a class="buttonDeleteSertif" data-id="' + data[2][i]
                                            .id +'"><i class="btn btn-sm icon-trash" ></i></a></div>');
                                        // $('#tampilsertifikat').append(
                                        //     '<i class="icon-trash"></i> icon-trash');
                                    }
                                    if (data[2].length >= 5) {
                                        $('#inputFileSertif').hide();
                                    } else {
                                        $('#inputFileSertif').show();
                                    }
                                }

                            // Data Section
                                $('#select_setion').empty();
                                $('#select_section').append('<option value=' + 0 + '>' +
                                    'Silakan Pilih Section' + '</option>');
                                for (var i = 0; i < data[3].length; i++) {

                                    $('#select_section').append('<option value=' + data[3][i].id + '>' +
                                        data[3][i].section + '</option>');

                                }

                            // Data Nilai

                        },
                        error: function(data) {
                            console.log('Error : ', data);
                        }
                    })

                });

                //Delete sertif
                $('body').on('click', '.buttonDeleteSertif',  function(){

                    Swal.fire({
                        title: 'Delete Sertif ?',
                        icon: 'question',
                        timer: 2000,
                        timerProgressBar: true,
                        showDenyButton: true,
                        showCancelButton: false,
                        confirmButtonText: `Delete`,
                        denyButtonText: `Don't Delete`,
                    }).then((result) => {
                        if (result.isConfirmed) {

                            var id = $(this).data("id");

                            $.ajax({
                                type: "get",
                                url: "/hapus/sertif/"+id,
                                success: function (data) {
                                    Swal.fire({
                                        title: 'Deleted',
                                        icon: 'success',
                                        timer: 1000,
                                        timerProgressBar: true,
                                        position: 'bottom-end',
                                        showConfirmButton: false,
                                    });

                                    $('#tampilsertifikat').empty();
                                    console.log('Data Foto : '+data);

                                    for (var i = 0; i < data.length; i++) {
                                        // $('#tampilsertifikat').val(fruits);
                                        $('#tampilsertifikat').append(
                                            '<div class="tes"><input style="margin:5px;border-radius: 10%;" class="btn btn-primary btn-sm" type="button" value="' +
                                            data[i].keterangan + '">' +
                                            '<a class="buttonDeleteSertif" data-id="' + data[i]
                                            .id +
                                            '"><i class="btn btn-sm icon-trash" ></i></a></div>');
                                        // $('#tampilsertifikat').append(
                                        //     '<i class="icon-trash"></i> icon-trash');
                                    }
                                    if (data.length >= 5) {
                                        $('#inputFileSertif').hide();
                                    } else {
                                        $('#inputFileSertif').show();
                                    }
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });

                            $('#inputFileSertif').show();

                        } else if (result.isDenied) {
                        Swal.fire('Canceled', '', 'info')
                        }
                    });

                })

                $('body').on('click','.buttonDetailSertifikat', function(){
                    $('#modalSertif').modal('show');

                    var id = $(this).data('id');

                    $.ajax({
                        type: 'POST',
                        url: '/detail/sertifikat',
                        data:{
                            id : id
                        },
                        beforeSend: function() {
                            $.blockUI({
                                baseZ: 2000,
                                message: '<i class="icon-spinner9 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(data){
                            $('#modalJudulSertif').val(data.keterangan);
                            $('#modalFotoSertifikat').empty();
                            $('#modalFotoSertifikat').append('<img style="width:100%;height:auto;" src="/sertif/'+data.file+'">');
                        },
                        error: function(data){
                            console.log('Error : ', data);
                        }
                    })
                })


                var fruits = [];
                var weight = [];
                var isLab=[];

                // ON Change Section
                $('#select_section').on('change', function(e) {
                    fruits = [];
                    var id = $('#select_section').val();
                    $("#header_param").show();
                    $("#headerdash").show();
                    $("#SectionReq").val();
                    $("#SectionReq").hide();
                    $("#SectionReq").show();
                    // console.log(id);
                    $.ajax({
                        url: "{{ route('input.getDataParameter') }}",
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function(data) {
                            console.log(data);
                            $('#Parameter').empty();
                            if(data[1]=='Lab'){
                                // var isLab = data[1];
                                for (var i = 0; i < data[0].length; i++) {

                                if(i==0){

                                    $('#Parameter').append('<label class' +
                                        '="text-semibold control-label col-sm-12">' + data[0][i]
                                        .param + '</label>' +
                                        ' <div class="col-md-12 col-lg-6 col-sm-12">' +
                                        '<input data-id="' + data[0][i].id + '" name="' + data[0][i]
                                        .param + '" type="number" min="0" max="100" class="form-control input-xl parameter inpot" value="" placeholder="Silahkan Masukkan Nilai 1-100">' +
                                        '<span id="SectionReq" class="help-block text-danger">*Required</span></div>'
                                    );
                                }
                                else{
                                    $('#Parameter').append('<label class' +
                                        '="text-semibold control-label col-sm-12">' + data[0][i]
                                        .param + '</label>' +
                                        ' <div class="col-md-12 col-lg-6 col-sm-12">' +
                                        '<input data-id="' + data[0][i].id + '" name="' + data[0][i]
                                        .param + '" type="number" min="4" max="' + 7 +
                                        '" class="form-control input-xl parameter inpot" value="" placeholder="Range Nilai 4-7" >' +
                                        '<span id="SectionReq" class="help-block text-danger">*Required</span></div>'
                                    );
                                }
                                    console.log(data[0][i].id);
                                    //idparamter masuk ke fruits
                                    fruits.push(data[0][i].id);
                                    weight.push(data[0][i].weight);
                                    isLab.push(data[1]);
                                    // console.log(fruits);


                                }
                            }
                            else{
                                for (var i = 0; i < data[0].length; i++) {
                                    $('#Parameter').append('<label class' +
                                        '="text-semibold control-label col-sm-12">' + data[0][i]
                                        .param + '</label>' +
                                        ' <div class="col-md-12 col-lg-6 col-sm-12">' +
                                        '<input data-id="' + data[0][i].id + '" name="' + data[0][i]
                                        .param + '" type="number" min="0" max="' + data[0][i].weight *
                                        100 +
                                        '" class="form-control input-xl parameter inpot" value="" placeholder="Silahkan Masukkan Nilai 1-' +
                                        data[0][i].weight * 100 + '" >' +
                                        '<span id="SectionReq" class="help-block text-danger">*Required</span></div>'
                                    );
                                    console.log(data[0][i].id);
                                    //idparamter masuk ke fruits
                                    fruits.push(data[0][i].id);
                                    weight.push(data[0][i].weight);
                                    // console.log(fruits);


                                }
                            }
                            // console.log(data);
                        }
                    })
                    // var countdata=data.length;



                });

                $('#SavePage').click(function() {

                    Swal.fire({
                        title: 'Do you want to save the changes?',
                        showDenyButton: true,
                        confirmButtonText: `Save`,
                        denyButtonText: `Don't save`,
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {

                            loading();

                            var nik = $('#select_nik').val();
                            var dataform = $('#formParam').serializeArray();
                            var section = $('#select_section').val();


                            if (!nik) {
                                // Swal.fire('NIK WAJIB DIISI!', '', 'info')
                                //     return false
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'NIK WAJIB DIISI!'
                                })
                                $.unblockUI();
                                return false

                            }
                            if (section == 0) {
                                // Swal.fire('NIK WAJIB DIISI!', '', 'info')
                                //     return false
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'SECTION WAJIB DIISI!'
                                })
                                $.unblockUI();
                                return false

                            }

                            if (!section) {
                                // Swal.fire('NIK WAJIB DIISI!', '', 'info')
                                //     return false
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'SECTION WAJIB DIISI!'
                                })
                                $.unblockUI();
                                return false

                            }

                            var Parameter = $('.parameter').val();

                            if (!Parameter) {

                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'PARAMETER WAJIB DIISI!'
                                })
                                $.unblockUI();
                                return false

                            } else {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                            'content')
                                    }
                                });
                                var Tolak = 0;
                                // console.log(isLab);
                                if(isLab[0]=='Lab'){

                                    for (var i = 0; i < dataform.length; i++) {
                                        // console.log(weight[1]);
                                        if(i==0){
                                            if (dataform[i].value > 100) {
                                                console.log('oke');
                                                var j=i+1;
                                                Tolak = 1;
                                                Swal.fire({
                                                    icon: 'warning',
                                                    title: 'Cek Lagi ya...',
                                                    text: 'Nilai parameter ke-'+j +' berada di luar range!'
                                                })
                                                $.unblockUI();
                                                return false

                                                // }
                                                // console.log('outofrange');
                                            }
                                        }
                                        else{

                                            if (dataform[i].value > 7) {
                                                console.log('oke');
                                                var j=i+1;
                                                Tolak = 1;
                                                Swal.fire({
                                                    icon: 'warning',
                                                    title: 'Cek Lagi ya...',
                                                    text: 'Nilai parameter ke-'+j +' berada di luar range!'
                                                })
                                                $.unblockUI();
                                                return false

                                                // }
                                                // console.log('outofrange');
                                            }
                                        }

                                    }
                                }
                                else{
                                     for (var i = 0; i < dataform.length; i++) {
                                        // console.log(weight[1]);

                                        if (dataform[i].value > (weight[i] * 100)) {
                                            console.log('oke');
                                            var j=i+1;
                                            Tolak = 1;
                                            Swal.fire({
                                                icon: 'warning',
                                                title: 'Cek Lagi ya...',
                                                text: 'Nilai parameter ke-'+j +' berada di luar range!'
                                            })
                                            $.unblockUI();
                                            return false

                                            // }
                                            // console.log('outofrange');
                                        }

                                    }
                                }
                                if (Tolak == 0) {
                                    $.ajax({
                                        type: 'post',
                                        url: "{{ route('input.form') }}",
                                        data: {
                                            nik: nik,
                                            section: section,
                                            dataform: dataform,
                                            fruitbuah: fruits
                                        },

                                        success: function(data) {
                                            Swal.fire('Saved!', '', 'success')
                                            $.unblockUI();
                                        },
                                        error: function(response) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Anda Sudah melakukan tes tadi!'
                                            })
                                            $.unblockUI();
                                            return false
                                        }
                                    });
                                }




                            }

                        } else if (result.isDenied) {
                            Swal.fire('Changes are not saved', '', 'info')
                        }
                    })



                });

                // Report section
                $('.select_section').select2();
                $(function() {
                    $('#reportfrom').datepicker();
                    $('#reportto').datepicker();
                });

                $('#export').click(function() {
                    $('#reportrange').show();
                    $('#reportfrom').show();
                    $('#reportto').show();
                    var selectedNIK = $('.select_nik').select2("data");
                    var nik = "";
                    // var section = "";
                    var from = $('#reportfrom').val();
                    var to = $('#reportto').val();

                    for (var i = 0; i < selectedNIK.length; i++) {
                        var str = selectedNIK[i].id;
                        nik = nik + str;
                    }

                    console.log(from);
                    console.log(to);
                    window.location.href = $('#export_xls').attr('href') + '?nik=' + nik + '&from=' + from +
                        '&to=' + to;


                });

                // History Section
                $('body').on('click', '.cekdetail', function() {

                    var id_skill = $(this).data('id');
                    var selectedNIK = $('.select_nik').select2("data");
                    var nik = selectedNIK[0].id;
                    console.log(nik);
                    $('#Modalinput').modal('show');
                    // loading();
                    setTbdetail(id_skill, nik);

                    // $('#Modalinput').unblock();

                });
                setModaloff();

                $("#Modalinput").on('hidden.bs.modal', function() {
                    $('#table_modal').DataTable().destroy();
                    $('#tabel_modal tbody').empty();
                });


                $('#showExcel').click(function() {
                    var selectedNIK = $('.select_nik').select2("data");
                    var nik = selectedNIK[0].id;
                    setTbExcel(nik);

                });

                function setTbSkill(nik) {

                    $.extend($.fn.dataTable.defaults, {
                        stateSave: true,
                        autoWidth: false,
                        autoLength: false,
                        processing: true,
                        serverSide: true,
                        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                        language: {
                            search: '<span>Filter:</span> _INPUT_',
                            searchPlaceholder: 'Type to filter...',
                            lengthMenu: '<span>Show:</span> _MENU_',
                            paginate: {
                                'first': 'First',
                                'last': 'Last',
                                'next': '&rarr;',
                                'previous': '&larr;'
                            }
                        }
                    });

                    var table = $('#table').DataTable({

                        ajax: {
                            type: 'GET',
                            url: "{{ route('input.getSkill') }}",
                            data: function(d) {
                                return $.extend({}, d, {
                                    "nik": nik
                                });
                            }
                        },

                        columns: [{
                                data: null,
                                sortable: false,
                                visible: false,
                                orderable: false,
                                searchable: false
                            },
                            {
                                data: 'idsection',
                                name: 'idsection'
                            },
                            {
                                data: 'total',
                                name: 'total',
                                searchable: false
                            },
                            {
                                data: 'created_at',
                                name: 'created_at'
                            },
                            {
                                data: 'code',
                                name: 'code',
                                searchable: false
                            },

                        ],
                        rowCallback: function(row, data, index) {

                            if (data.total == 'A' || data.total == 'B') {
                                $(row).css('background-color', '#cbf2aa');
                            } else {
                                $(row).css('background-color', '#f2aaaa');
                            }
                        }
                    });


                }

                function setTbdetail(id_skill, nik) {

                    var table_detail = $('#table_modal').DataTable({

                        serverSide: true,
                        ajax: {
                            type: 'GET',
                            url: "{{ route('input.getSkillDetail') }}",
                            data: function(d) {
                                return $.extend({}, d, {
                                    "id_skill": id_skill,
                                    "nik": nik
                                });
                            },
                            beforeSend: function() {
                                $.blockUI({
                                    baseZ: 2000,
                                    message: '<i class="icon-spinner9 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: 'rgba(211, 209, 206, 0.5)',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function() {
                                $.unblockUI();
                            },

                        },
                        language: {
                            processing: "<span class='fa-stack fa-lg'>\n\
                                                <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                                        </span>&emsp;Processing ..."

                        },

                        columns: [{
                                data: null,
                                sortable: false,
                                visible: false,
                                orderable: false,
                                searchable: false
                            },
                            {
                                data: 'created_at',
                                name: 'created_at'
                            },
                            {
                                data: 'idparameter',
                                name: 'idparameter'
                            },
                            {
                                data: 'nilai',
                                name: 'nilai'
                            },
                            {
                                data: 'standar',
                                name: 'standar'
                            },

                        ],
                        rowCallback: function(row, data, index) {

                            if (data.nilai.substring(0, 1) == 'A') {
                                $(row).css('background-color', '#cbf2aa');
                            } else {
                                $(row).css('background-color', '#f2aaaa');
                            }
                        },

                    })

                    // $.unblockUI()


                    // $('#Modalinput').unblock({
                    // // message: '<h1>Processing</h1>'
                    // });


                }
                function setModaloff() {
                     $('#Modalinput').unblock();
                }

                function setTbExcel(nik) {

                    var from = $('#reportfrom').val();
                    var to = $('#reportto').val();
                    var table_detail = $('#table-report-preview').DataTable({
                        ajax: {
                            type: 'GET',
                            url: "{{ route('input.getTabelExcel') }}",
                            data: function(d) {
                                return $.extend({}, d, {
                                    "nik": nik,
                                    "from": from,
                                    "to": to,
                                });
                            }

                        },


                        columns: [{
                                data: 'created_at',
                                name: 'created_at'
                            },
                            {
                                data: 'total',
                                name: 'total'
                            },
                            {
                                data: 'weight',
                                name: 'weight'
                            },
                            {
                                data: 'huruf',
                                name: 'huruf'
                            },
                            {
                                data: 'standar',
                                name: 'standar'
                            },
                            {
                                data: 'section',
                                name: 'section'
                            },
                            {
                                data: 'param',
                                name: 'param'
                            },


                        ],
                        // rowCallback: function(row, data, index) {

                        //     if (data.nilai.substring(0, 1) == 'A') {
                        //         $(row).css('background-color', '#cbf2aa');
                        //     } else {
                        //         $(row).css('background-color', '#f2aaaa');
                        //     }
                        // }
                    });


                }

                //     var table = $('#table-report-preview').DataTable({
                //     processing: true,
                //     serverSide: true,
                //     responsive: true,
                //     ajax: {
                //                     type: 'GET',
                //                     url: "{{ route('input.getTabelExcel') }}",
                //                     data: function(d) {
                //                         return $.extend({}, d, {

                //                             "nik": nik
                //                         });
                //                     }

                //                 },
                //     columns: [
                //         {data: 'created_at', name: 'created_at'},
                //         {data: 'total', name: 'total'},
                //         {data: 'weight', name: 'weight'},
                //         {data: 'huruf', name: 'huruf'},
                //         {data: 'standar', name: 'standar'},
                //         {data: 'section', name: 'section'},
                //         {data: 'param', name: 'param'},
                //         // {data: 'action', name: 'action', orderable: false, searchable: false},
                //     ]

                // });


            });
        </script>
    @endsection

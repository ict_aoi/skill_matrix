@extends('layouts.app', ['active' => 'input'])
@section('header')
    <div class="page-header page-header-default">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="#"><i class="icon-home2 position-left"></i> Input</a></li>
                <li class="active">Input Matrix</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="content">
        <div class="row">
            {{-- <h1>coba</h1> --}}
            <div class=" panel panel-flat">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group " id="SectionDiv">

                            <h1>Masukan NIK / Nama</h1>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <select class="form-control select-search select2-hidden-accessible select_nik"
                                    id="select_nik" name="nik[]" tabindex="-1" aria-hidden="true">

                                </select>

                            </div>

                        </div>
                    </div>

                    <div class="row form-group">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row" id=dataskill>
                                    <h1> Data Karyawan</h1>
                                    <div class="col-lg-6">

                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>NIK</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="nikk" readonly>

                                        </div>
                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Nama</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="name" readonly>

                                        </div>
                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Departemen</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="department" readonly>

                                        </div>
                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Subdept</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="subdept" readonly>
                                        </div>
                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Position</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="position" readonly>

                                        </div>

                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Factory</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="factory" readonly>

                                        </div>
                                        <div class=" col-lg-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text "><b>Masa Kerja</b></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="masakerja" readonly>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <form id="form-foto" method="POST" enctype="multipart/form-data"
                                            action="/upload/proses">
                                            {{ csrf_field() }}
                                            <div class=" col-lg-3">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text "><b>Foto</b></div>
                                                </div>



                                            </div>
                                            <div class="col-lg-9">
                                                <div class="input-group-text " id="foto"></div>
                                                {{-- <input type="text" class="form-control" id="department" readonly> --}}
                                            </div>
                                            <div class="row">

                                                <div class=" col-lg-3">
                                                    <div class="input-group-prepend">
                                                        {{-- <div class="input-group-text "><b>Ganti Foto</b></div> --}}
                                                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input type="file" name="filee" placeholder="Choose File" id="filee">
                                                    <span class="help-block text-danger">*ekstensi gambar harus jpeg,jpg
                                                        dengan size max 5 mb</span>
                                                    {{-- <input type="text" class="form-control" id="nikkk" hidden> --}}

                                                    <div class="form-group">
                                                        <b hidden>NIK yg dikirim</b>
                                                        <textarea class="form-control hidden" id="nikkk"
                                                            name="nik"></textarea>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <button class="btn btn-success upload-image hidden"
                                                            type="submit">Upload Image</button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <button class="btn btn-success upload-image hidden"
                                                            type="submit">Upload Image</button>
                                                    </div>
                                                </div>

                                            </div>
                                            {{-- <div class="col-lg-1">

                                                </div> --}}
                                            {{-- </form> --}}

                                            {{-- <form id="form-sertif" method="POST" enctype="multipart/form-data" action="/upload/proses/sertif"> --}}
                                            {{-- {{ csrf_field() }} --}}
                                            <div class="row">

                                                <div class=" col-lg-3">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text "><b>Sertifikat</b></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="input-group-text " id="tampilsertifikat"></div>


                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class=" col-lg-3">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text hidden"><b>Tambahkan</b></div>
                                                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input type="file" name="file[]" placeholder="Choose File" id="file"
                                                        size="40" class="form-control" multiple="">

                                                    {{-- <input type="file" name="berkasraporttab6" id="berkasraporttab6" size="40" onchange="myFunctions2('berkasraporttab6','5242880')" class="form-control" style="font-weight:normal;font-size:16px;font-family:Book Antiqua" accept="image/jpg, image/jpeg, image/png, image/gif, image/bmp, application/pdf"> --}}
                                                    {{-- <input type="file" name="profileImage[]" class="form-control" multiple=""> --}}
                                                    {{-- <input type="text" class="form-control" id="nikkk" hidden> --}}
                                                    <div class="form-group">
                                                        <b hidden>NIK yg dikirim</b>
                                                        <textarea class="form-control" id="nikkkk" name="nik"></textarea>
                                                        {{-- <textarea class="form-control" id="keterangan" name="keterangan" ></textarea> --}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class=" col-lg-3">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text "><b>Judul Sertifikat</b></div>
                                                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">

                                                    <div class="form-group">

                                                        <textarea class="form-control" id="keterangan" name="keterangan[]"
                                                            multiple=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <div class="form-group">
                                                            <button class="btn btn-success upload-image "
                                                                type="submit">Upload</button>
                                                        </div>
                                                    </div>


                                                </div>


                                            </div>
                                            {{-- <div class="col-lg-1">

                                                </div> --}}
                                        </form>




                                    </div>




                                </div>


                            </div>
                        </div>


                        <div class="form-group " id="SectionDiv">
                            <h1>
                                Pilih Section
                            </h1>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <select class="form-control select-search select2-hidden-accessible" id="select_section"
                                    name="section" tabindex="-1" aria-hidden="true">

                                </select>
                                <span class="SectionReq help-block text-danger">*Harap isi NIK terlebih dahulu</span>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <h1 id="header_param">Input Paramater</h1>
                        <div class="col-lg-12">

                            <div class="row">
                                <form id="formParam">
                                    <div class="form-group" id="Parameter"></div>
                                </form>


                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-2 "></div>
                            <div class="col-lg-2"></div>
                            <button id="SavePage" name="SavePage" class="btn btn-primary col-lg-4">Save <i
                                    class="icon-floppy-disk"></i></button>
                            <div class="col-lg-2"></div>



                        </div>
                    </div>

                </div>








            </div>

        @endsection

        @section('js')
            <script type="text/javascript">
                $(document).ready(function() {

                    $('.select_nik').select2();

                    $("#header_param").hide();

                    $("#headerdash").hide();
                    $(".SectionReq").show();


                    $.ajax({
                        type: 'get',
                        url: "{{ route('input.NIK') }}",

                        success: function(data) {

                            $('#select_nik').empty();
                            $('#select_nik').append('<option value=' + 0 + '>' + 'Silakan Pilih NIK / Nama' +
                                '</option>');
                            for (var i = 0; i < data.length; i++) {

                                $('#select_nik').append('<option value=' + data[i].nik + '>' + data[i].nik +
                                    ' - ' + data[i].name + '</option>');

                            }


                        },
                        error: function(response) {
                            console.log('tidakoke');
                        }
                    });




                    var table = $('#table-list').DataTable({
                        processing: true,
                        serverSide: true,
                        deferRender: true,
                        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                        language: {
                            search: '<span>Filter:</span> _INPUT_',
                            searchPlaceholder: 'Type to filter...',
                            lengthMenu: '<span>Show:</span> _MENU_',
                            paginate: {
                                'first': 'First',
                                'last': 'Last',
                                'next': '&rarr;',
                                'previous': '&larr;'
                            }
                        },
                        ajax: {
                            type: 'GET',
                            url: "{{ route('master.getDataMachine') }}"
                        },
                        fnCreatedRow: function(row, data, index) {
                            var info = table.page.info();
                            var value = index + 1 + info.start;
                            $('td', row).eq(0).html(value);
                        },
                        columns: [{
                                data: null,
                                sortable: false,
                                orderable: false,
                                searchable: false
                            },
                            {
                                data: 'machine_name',
                                name: 'machine_name'
                            },
                            {
                                data: 'machine_group',
                                name: 'machine_group'
                            },
                            {
                                data: 'machine_category',
                                name: 'machine_category'
                            }
                        ]
                    });


                    $('#select_nik').on('change', function(e) {
                        $('#Modalinput').modal('show');
                        var nik = $('#select_nik').val();
                        console.log(nik);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: 'post',
                            url: "{{ route('input.tampilfoto') }}",
                            data: {
                                nik: nik
                            },
                            success: function(data) {

                                // console.log(data);
                                $('#foto').empty();

                                var img = document.createElement("img");
                                if (data[0].file) {
                                    data = data[0].file;
                                    esrc = "/data_file/";
                                    esrc = esrc.concat(data);
                                } else {
                                    data = data;
                                    esrc = "/data_file/";
                                    esrc = esrc.concat(data);
                                }


                                // console.log(esrc);
                                img.src = esrc;
                                var src = document.getElementById("foto");
                                img.style.width = "150px";
                                src.appendChild(img);


                            },
                            error: function(response) {
                                console.log('tidakoke');
                            }
                        });
                        // document.getElementById("tampilsertifikat").value = "1";
                        $.ajax({
                            type: 'post',
                            url: "{{ route('input.tampilsertifikat') }}",
                            data: {
                                nik: nik
                            },
                            success: function(data) {
                                if (data == 'Belum Upload Sertifikat') {
                                    $('#tampilsertifikat').empty();
                                    $('#tampilsertifikat').append('<div class="input-group-text ">' +
                                        data + '</div>');
                                } else {

                                    fruits = [];
                                    $('#tampilsertifikat').val();
                                    $('#tampilsertifikat').empty()
                                    for (var i = 0; i < data.length; i++) {
                                        console.log(data[i].keterangan);
                                        fruits.push(data[i].keterangan);
                                        // $('#tampilsertifikat').val(data[i].keterangan);
                                        // document.getElementById("tampilsertifikat").value = "1";
                                        // $('#tampilsertifikat').append('<option disabled value=' + data[i].keterangan + '>' + data[i].keterangan + '</option>');
                                    }
                                    for (var i = 0; i < data.length; i++) {
                                        // $('#tampilsertifikat').val(fruits);
                                        $('#tampilsertifikat').append(
                                            '<input style="margin:5px;border-radius: 10%;" class="btn btn-primary btn-sm" type="button" value="' +
                                            data[i].keterangan + '">');

                                    }
                                }
                                // var implode = fruits.join(",");
                                // console.log(implode);
                                // $('#tampilsertifikat').val(implode);

                                //   $('#tampilsertifikat').val();
                                //     console.log(oke);


                            },
                            error: function(response) {
                                console.log('tidakoke');
                            }
                        });
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('input.getDataQC') }}",
                            data: {
                                nik: nik
                            },

                            success: function(data) {
                                //     console.log('oke');
                                // console.log(data);
                                $('#nikk').val(data[0].nik);
                                $('#nikkkk').val(data[0].nik);
                                $('#name').val(data[0].name);
                                $('#department').val(data[0].department_name);
                                $('#subdept').val(data[0].subdept_name);
                                $('#position').val(data[0].position);
                                $('#factory').val(data[0].factory);

                                //tahun
                                var masakerja = data[0].masakerja;
                                var masakerjatahun = '20'.concat(masakerja.substring(0, 2));
                                var today = new Date();
                                var yearnow = today.getFullYear();
                                masakerjatahun = parseInt(masakerjatahun)
                                var berapatahun = (yearnow - masakerjatahun);


                                //bulan
                                var masakerjabulan = masakerja.substring(2, 4);
                                if (masakerjabulan.substring(0, 1) == 0) {
                                    masakerjabulan = masakerjabulan.substring(1, 4);
                                }
                                var today = new Date();
                                var monthnow = today.getMonth() + 1;
                                masakerjabulan = parseInt(masakerjabulan)


                                var berapabulan = (12 + (monthnow - masakerjabulan));
                                if (berapabulan == 12) {
                                    berapatahun = berapatahun + 1;
                                    berapabulan = 0;
                                }

                                berapatahun = berapatahun.toString().concat(' tahun')
                                berapabulan = berapabulan.toString().concat(' bulan')
                                var masakerjatotal = berapatahun.concat(' ', berapabulan);


                                $('#masakerja').val(masakerjatotal);


                            },
                            error: function(response) {
                                console.log(response);
                            }
                        });
                        $.ajax({
                            type: 'get',
                            url: "{{ route('input.getDataSection') }}",

                            success: function(data) {

                                $('#select_setion').empty();
                                $('#select_section').append('<option value=' + 0 + '>' +
                                    'Silakan Pilih Section' + '</option>');
                                for (var i = 0; i < data.length; i++) {

                                    $('#select_section').append('<option value=' + data[i].id + '>' +
                                        data[i].section + '</option>');

                                }


                            },
                            error: function(response) {
                                console.log('tidakoke');
                            }
                        });
                    });


                    var fruits = [];
                    var weight = [];

                    $('#select_section').on('change', function(e) {
                        fruits = [];
                        var id = $('#select_section').val();
                        $("#header_param").show();
                        $("#headerdash").show();
                        $("#SectionReq").val();
                        $("#SectionReq").hide();
                        $("#SectionReq").show();
                        // console.log(id);
                        $.ajax({
                            url: "{{ route('input.getDataParameter') }}",
                            type: "POST",
                            data: {
                                id: id
                            },
                            success: function(data) {
                                $('#Parameter').empty();
                                for (var i = 0; i < data.length; i++) {
                                    $('#Parameter').append('<label class' +
                                        '="text-semibold control-label col-sm-12">' + data[i]
                                        .param + '</label>' +
                                        ' <div class="col-md-12 col-lg-6 col-sm-12">' +
                                        '<input data-id="' + data[i].id + '" name="' + data[i]
                                        .param + '" type="number" min="0" max="' + data[i].weight *
                                        100 +
                                        '" class="form-control input-xl parameter inpot" value="" placeholder="Silahkan Masukkan Nilai 1-' +
                                        data[i].weight * 100 + '" >' +
                                        '<span id="SectionReq" class="help-block text-danger">*Required</span></div>'
                                        );
                                    console.log(data[i].id);
                                    //idparamter masuk ke fruits
                                    fruits.push(data[i].id);
                                    weight.push(data[i].weight);
                                    // console.log(fruits);


                                }
                                // console.log(data);
                            }
                        })
                        // var countdata=data.length;



                    });



                    $('#SavePage').click(function() {

                        Swal.fire({
                            title: 'Do you want to save the changes?',
                            showDenyButton: true,
                            confirmButtonText: `Save`,
                            denyButtonText: `Don't save`,
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {

                                loading();

                                var nik = $('#select_nik').val();
                                var dataform = $('#formParam').serializeArray();
                                var section = $('#select_section').val();


                                if (!nik) {
                                    // Swal.fire('NIK WAJIB DIISI!', '', 'info')
                                    //     return false
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'NIK WAJIB DIISI!'
                                    })
                                    $.unblockUI();
                                    return false

                                }
                                if (section == 0) {
                                    // Swal.fire('NIK WAJIB DIISI!', '', 'info')
                                    //     return false
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'SECTION WAJIB DIISI!'
                                    })
                                    $.unblockUI();
                                    return false

                                }

                                if (!section) {
                                    // Swal.fire('NIK WAJIB DIISI!', '', 'info')
                                    //     return false
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'SECTION WAJIB DIISI!'
                                    })
                                    $.unblockUI();
                                    return false

                                }

                                var Parameter = $('.parameter').val();

                                if (!Parameter) {

                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'PARAMETER WAJIB DIISI!'
                                    })
                                    $.unblockUI();
                                    return false

                                } else {
                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                                'content')
                                        }
                                    });
                                    var Tolak = 0;
                                    for (var i = 0; i < dataform.length; i++) {
                                        // console.log(weight[1]);
                                        if (dataform[i].value > (weight[i] * 100)) {
                                            console.log('oke');

                                            Tolak = 1;
                                            Swal.fire({
                                                icon: 'warning',
                                                title: 'Cek Lagi ya...',
                                                text: 'Nilai parameter berada di luar range!'
                                            })
                                            $.unblockUI();
                                            return false

                                            // }
                                            // console.log('outofrange');
                                        }

                                    }
                                    if (Tolak == 0) {
                                        $.ajax({
                                            type: 'post',
                                            url: "{{ route('input.form') }}",
                                            data: {
                                                nik: nik,
                                                section: section,
                                                dataform: dataform,
                                                fruitbuah: fruits
                                            },

                                            success: function(data) {
                                                Swal.fire('Saved!', '', 'success')
                                                $.unblockUI();
                                            },
                                            error: function(response) {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Oops...',
                                                    text: 'Anda Sudah melakukan tes tadi!'
                                                })
                                                $.unblockUI();
                                                return false
                                            }
                                        });
                                    }




                                }

                            } else if (result.isDenied) {
                                Swal.fire('Changes are not saved', '', 'info')
                            }
                        })



                    });




                });
            </script>
        @endsection

<ul class="navigation navigation-main navigation-accordion">

	<!-- Main -->
	@permission(['menu-dashboard'])
	<li class="{{ $active == 'dashboard' ? 'active' : ''}}"><a href="{{ route('home.index') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
	@endpermission

	@permission(['menu-user-management'])
	<li class="navigation-header"><span>User Management</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'user_account' ? 'active' : ''}}"><a href="{{ route('admin.user_account')}}"><i class="icon-theater"></i> <span>User Account</span></a></li>
	<li class="{{ $active == 'role_user' ? 'active' : ''}}"><a href="{{route('admin.formRole')}}"><i class="icon-menu2"></i> <span>Role</span></a></li>
	@endpermission

	@permission(['menu-master'])
	<li class="navigation-header"><span>Master</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'master_mesin' ? 'active' : ''}}"><a href="{{ route('master.mmachine')}}"><i class="icon-cog"></i> <span>Master Machine</span></a></li>
	<li class="{{ $active == 'master_product' ? 'active' : ''}}"><a href="{{ route('master.mprodstyle')}}"><i class="icon-price-tags"></i> <span>Master Product & Style</span></a></li>
	<li class="{{ $active == 'master_proses' ? 'active' : ''}}"><a href="{{ route('master.mproses')}}"><i class="icon-reset"></i> <span>Master Proses</span></a></li>
	<li class="{{ $active == 'master_operation_complex' ? 'active' : ''}}"><a href="{{ route('master.mcomplexity') }}"><i class="icon-color-sampler"></i> <span>Master Operation Complexity</span></a></li>
	<li class="{{ $active == 'master_sewer' ? 'active' : ''}}"><a href="{{ route('master.msewer') }}"><i class="icon-users4"></i> <span>Master Sewer</span></a></li>
    <li class="{{ $active == 'master_qc' ? 'active' : ''}}"><a href="{{ route('master.mqc') }}"><i class="icon-people"></i> <span>Master QC</span></a></li>
    <li class="{{ $active == 'master_section' ? 'active' : ''}}"><a href="{{ route('master.section') }}"><i class="icon-people"></i> <span>Master Section</span></a></li>
    <li class="{{ $active == 'master_parameter' ? 'active' : ''}}"><a href="{{ route('master.parameter') }}"><i class="icon-people"></i> <span>Master Parameter</span></a></li>

	@endpermission

    @permission(['menu-input-qc'])
	<li class="navigation-header"><span>Input</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'input' ? 'active' : ''}}"><a href="{{ route('input.index')}}"><i class="icon-pencil5"></i> <span>Input Penilaian</span></a></li>
	<li class="{{ $active == 'history' ? 'active' : ''}}"><a href="{{ route('input.history')}}"><i class=" icon-history"></i> <span>History</span></a></li>
	<li class="{{ $active == 'report' ? 'active' : ''}}"><a href="{{ route('input.report')}}"><i class=" icon-file-download"></i> <span>Report</span></a></li>
    <li class="{{ $active == 'summary' ? 'active' : ''}}"><a href="{{ route('input.summary')}}"><i class=" icon-file-check2"></i> <span>Summary</span></a></li>


	@endpermission

	<!-- /page kits -->

</ul>

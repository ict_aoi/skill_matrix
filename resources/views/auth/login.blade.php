<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"  href="{{url('assets/icon/matrix.png')}}">
    <title>Skill Matrix</title>

    <!-- Global stylesheets -->
    <link href="{{ url('assets/css/googleapis.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{url('assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/pages/login.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/plugins/ui/ripple.min.js')}}"></script>

    <!-- /theme JS files -->
    <style>
        .bg-img {
  /* The image used */
            background-image: url("{{url('assets/icon/factorydata22.jpg')}}");

            min-height: 380px;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }

        div .content{
            opacity: 0.9;
        }

        #btn-login{
            opacity: 1.0;
        }
    </style>
</head>

<!-- <body class="login-container" class="login-container" style="background-image: url('assets/icon/factorydata22.jpg'); background-position: center; background-repeat: no-repeat; height: 100%;  background-size: cover; "> -->

<body class="login-container" class="login-container"  ">
<div class="bg-img">

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Advanced login -->
                    <form action="{{route('login')}}" id="form-login" method="POST">
                         @csrf
                        <div class="panel panel-body login-form">
                            {{-- <div class="text-center">
                                <h3>Skill Matrix</h3>
                            </div> --}}
                            <div class="text-center">
                                <!-- <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div> -->
                                <img src="{{url('assets/icon/logo_BBI.png')}}" width="150">
                                <h5 class="content-group">Login to your account</h5>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control {{ $errors->has('nik') ? ' is-invalid' : '' }}" placeholder="NIK" id="nik" name="nik" value="{{ old('nik') }}" required="" autofocus >
                                @if ($errors->has('nik'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nik') }}</strong>
                                    </span>
                                @endif
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" id="password" name="password" required="">
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger fade in">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                                <br>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn bg-pink-400 btn-block" id="btn-login">Login <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </form>
                    <!-- /advanced login -->




                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
</div>



</body>
{{-- <script>
    function shakeForm() {
   var l = 20;

   for( var i = 0; i <= 10; i++ ) {
     $( 'form' ).animate( {
         'margin-left': '+=' + ( l = -l ) + 'px',
         'margin-right': '-=' + l + 'px'
      }, 50);
   }
}
</script> --}}
</html>

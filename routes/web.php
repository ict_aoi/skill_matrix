<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
    	return redirect()->route('home.index');
    }

    return redirect('/login');
});

Route::get('/auth', 'Auth\LoginController@showLogin')->name('auth.showLogin');
Route::post('/auth/loginaction', 'Auth\LoginController@loginAction')->name('auth.loginAction');
Route::post('/auth/logoutaction', 'Auth\LoginController@logoutAction')->name('auth.logoutAction');

Auth::routes();

Route::middleware('auth')->group(function(){
	Route::prefix('/dashboard')->group(function(){
		Route::get('', 'DashboardController@index')->name('home.index');
	});


	//account setting
	Route::prefix('/user')->group(function(){
		Route::get('/myAccount', 'User\AccountController@myAccount')->name('account.myAccount');
		Route::post('/myAccount/edit', 'User\AccountController@editPassword')->name('account.editPassword');
	});

	Route::prefix('/admin')->middleware(['permission:menu-user-management'])->group(function(){
		//user account
		Route::get('/user-account', 'Admin\AdminController@user_account')->name('admin.user_account');
		Route::get('/user-account/get-data', 'Admin\AdminController@getDataUser')->name('admin.getDataUser');
		Route::get('/user-account/ajaxGetFactory', 'Admin\AdminController@ajaxGetFactory')->name('admin.ajaxGetFactory');
        Route::get('/user-account/ajaxGetRole', 'Admin\AdminController@ajaxGetRole')->name('admin.ajaxGetRole');
		Route::get('/user-account/add-user', 'Admin\AdminController@addUser')->name('admin.addUser');
		Route::get('/user-account/edit', 'Admin\AdminController@formEditUser')->name('admin.formEditUser');
		Route::get('/user-account/edit/edit-user', 'Admin\AdminController@editAccount')->name('admin.editAccount');
		Route::get('/user-account/edit/reset-password', 'Admin\AdminController@passwordReset')->name('admin.passwordReset');
		Route::get('/user-account/innactive', 'Admin\AdminController@innactiveUser')->name('admin.innactiveUser');

		//role
		Route::get('/role', 'Admin\AdminController@formRole')->name('admin.formRole');
		Route::get('/role/get-data', 'Admin\AdminController@ajaxRoleData')->name('admin.ajaxRoleData');
		Route::get('/role/new-role', 'Admin\AdminController@newRole')->name('admin.newRole');
		Route::post('/role/add-role', 'Admin\AdminController@addRole')->name('admin.addRole');
		Route::get('/role/edit', 'Admin\AdminController@editRoleUser')->name('admin.editRoleUser');
		Route::post('/role/update-role', 'Admin\AdminController@updateRole')->name('admin.updateRole');
	});

	Route::prefix('/master')->middleware(['permission:menu-master'])->group(function(){
		Route::get('/master-machine', 'Master\MasterController@mmachine')->name('master.mmachine');
		Route::get('/master-machine/get-data', 'Master\MasterController@getDataMachine')->name('master.getDataMachine');


		Route::get('/master-product', 'Master\MasterController@mprodstyle')->name('master.mprodstyle');
		Route::get('/master-product/get-data', 'Master\MasterController@getDataProdstle')->name('master.getDataProdstle');


		Route::get('/master-proses', 'Master\MasterController@mproses')->name('master.mproses');
		Route::get('/master-proses/get-data', 'Master\MasterController@getDataProcess')->name('master.getDataProcess');

		Route::get('/master-complexity', 'Master\MasterController@mcomplexity')->name('master.mcomplexity');
		Route::get('/master-complexity/get-data', 'Master\MasterController@getDataComplex')->name('master.getDataComplex');

		Route::get('/master-sewer', 'Master\MasterController@msewer')->name('master.msewer');
		Route::get('/master-sewer/get-data', 'Master\MasterController@getDataSewer')->name('master.getDataSewer');

        Route::get('/master-qc', 'Master\MasterController@mqc')->name('master.mqc');
		Route::get('/master-qc/get-data', 'Master\MasterController@getDataQC')->name('master.getDataQC');

        Route::get('/master-section', 'Master\SectionController@m_section')->name('master.section');
        Route::get('/master-section/get-data', 'Master\SectionController@getDataSection')->name('master.getDataSection');
        Route::get('/master-section/create', 'Master\SectionController@getDataCreateSection')->name('master.getDataCreateSection');
        Route::post('/master-section/insert', 'Master\SectionController@insertSection')->name('master.insertSection');
        Route::post('/master-section/edit', 'Master\SectionController@editSection')->name('master.editSection');
        Route::post('/master-section/update', 'Master\SectionController@updateSection')->name('master.updateSection');
        Route::get('/master-section/delete/{id}', 'Master\SectionController@deleteSection');

        Route::get('/master-parameter', 'Master\ParameterController@mparameter')->name('master.parameter');
        Route::get('/master-parameter/get-data', 'Master\ParameterController@getData')->name('master.getDataParameter');
        Route::post('/master-parameter/insert-parameter', 'Master\ParameterController@insertParameter')->name('master.insertParameter');
        Route::post('/master-parameter/edit-parameter', 'Master\ParameterController@editParameter')->name('master.editParameter');

        Route::post('/master-parameter/edit-parameter-sec', 'Master\ParameterController@editParameterSec')->name('master.editParameterSec');
        Route::post('/master-parameter/update-parameter', 'Master\ParameterController@updateParameter')->name('master.updateParameter');
        Route::post('/master-parameter/delete-parameter', 'Master\ParameterController@deleteParameter')->name('master.deleteParameter');
	});

    Route::prefix('/input')->middleware(['permission:menu-input-qc'])->group(function(){
		Route::get('/input-form', 'Input\InputController@index')->name('input.index');
		Route::post('/get-input-data', 'Input\InputController@getInputData')->name('input.getInputData');
        Route::get('/history', 'Input\InputController@getHistoryData')->name('input.history');
        Route::get('/history/data', 'Input\InputController@getHistoryTrans')->name('input.getDataHistory');
        Route::post('/master-qc/get-data', 'Input\InputController@getDataQC')->name('input.getDataQC');
        Route::get('/master-qc/get-data-section', 'Input\InputController@getDataSection')->name('input.getDataSection');
        Route::get('/get-data-skill', 'Input\InputController@getSkill')->name('input.getSkill');
        Route::post('/master-qc/get-data-parameter', 'Input\InputController@getDataParameter')->name('input.getDataParameter');
        Route::post('/master-qc/inputform', 'Input\InputController@InputForm')->name('input.form');
        Route::get('/master-qc/getNilaiDetail', 'Input\InputController@getSkillDetail')->name('input.getSkillDetail');
        Route::get('/report', 'Input\InputController@getReport')->name('input.report');
        Route::get('/report/getNIK', 'Input\InputController@getNIK')->name('input.NIK');
        Route::get('/report/getExcel', 'Input\ExcelController@getExcel')->name('input.getExcel');
        Route::get('/report/getTabelExcel', 'Input\ExcelController@getTabelExcel')->name('input.getTabelExcel');
        Route::get('/summary', 'Input\SummaryController@index')->name('input.summary');
        Route::get('/getSummary', 'Input\SummaryController@getSummary')->name('input.getSummary');
        Route::post('/get-parameter', 'Input\SummaryController@getDataParameter')->name('input.getParameter');
    });

    Route::get('/upload', 'Input\UploadController@upload');
    Route::post('/upload/proses', 'Input\UploadController@proses_upload')->name('input.foto');
    Route::post('/upload/tampil', 'Input\UploadController@TampilFoto')->name('input.tampilfoto');
    // Route::post('/upload/proses/sertif', 'Input\UploadController@proses_upload_sertif')->name('input.sertif');
    Route::post('/upload/tampilsertifikat', 'Input\UploadController@TampilSertif')->name('input.tampilsertifikat');
    Route::get('/hapus/sertif/{id}', 'Input\UploadController@HapusSertif')->name('input.hapussertifikat');
    Route::post('/detail/sertifikat', 'Input\UploadController@DetailSertif')->name('input.detailsertifikat');





});

Route::prefix('/cron')->group(function(){
	Route::get('/master/product', 'Master\CronController@m_product')->name('cron.mproduct');
	Route::get('/master/style', 'Master\CronController@m_style')->name('cron.mstyle');
	Route::get('/master/machine', 'Master\CronController@m_machine')->name('cron.mmachine');
	Route::get('/master/complexity', 'Master\CronController@m_complex')->name('cron.mcomplex');
	Route::get('/master/process', 'Master\CronController@m_proses')->name('cron.mproses');
	Route::get('/master/sewer', 'Master\CronController@msewer')->name('cron.msewer');
	Route::get('/master/workflow', 'Master\CronController@m_workflow')->name('cron.mworkflow');
	Route::get('/master/line', 'Master\CronController@mline')->name('cron.mline');
});

Route::get('/tes', function () {
    return view('Input\select');
});




